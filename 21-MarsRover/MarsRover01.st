!classDefinition: #MarsRoverTest category: #MarsRover01!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: 'rover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

!MarsRoverTest methodsFor: 'setUp/tearDown' stamp: 'AC 10/10/2019 05:13:42'!
setUp
	rover := MarsRover withPosition: 0@0 andDirection: North .
	! !


!MarsRoverTest methodsFor: 'avance y retroceso' stamp: 'AC 10/10/2019 05:13:42'!
"
Encaro este desarrollo por el camino de enviar el mismo comando muchas veces
"
test01SecuenciaDeComandosVacia

	rover do: '' .
	
	self assertRover: rover isInPosition: 0@0 heading: North! !

!MarsRoverTest methodsFor: 'avance y retroceso' stamp: 'AC 10/10/2019 05:13:42'!
test02AvanzarUnaVez

	rover do: 'f' .
	

	self assertRover: rover isInPosition: 0@1 heading: North.! !

!MarsRoverTest methodsFor: 'avance y retroceso' stamp: 'AC 10/10/2019 05:13:42'!
test03AvanzarMuchasVeces

	rover do: 'ff' .
	
	self assertRover: rover isInPosition: 0@2 heading: North! !

!MarsRoverTest methodsFor: 'avance y retroceso' stamp: 'AC 10/10/2019 05:13:42'!
test04AvanzarYRetroceder

	rover do: 'ffb' .
	
	self assertRover: rover isInPosition: 0@1 heading: North! !


!MarsRoverTest methodsFor: 'validacion de comandos' stamp: 'AC 10/10/2019 05:13:42'!
test05AvanzarYComandoInvalido

	self should: [ rover do: 'ffxfbb' . ] 
	raise: Error 
	withExceptionDo: [ :anError | self assert: MarsRover errorComandoNoValido equals: anError messageText . ] .
	
	self assertRover: rover isInPosition: 0@2 heading: North
! !


!MarsRoverTest methodsFor: 'giros y maniobras' stamp: 'AC 10/10/2019 05:13:14'!
test06GirarADerecha

	rover do: 'r' .
	
	self assertRover: rover isInPosition: 0@0 heading: East! !

!MarsRoverTest methodsFor: 'giros y maniobras' stamp: 'AC 10/10/2019 05:13:14'!
test07GirarADerechaYAvanzar

	rover do: 'rf' .
	
	self assertRover: rover isInPosition: 1@0 heading: East! !

!MarsRoverTest methodsFor: 'giros y maniobras' stamp: 'AC 10/10/2019 05:13:59'!
test08GiroADerechaConAvancesYRetrocesos

	rover do: 'rfbbr' .
	
	self assertRover: rover isInPosition: -1@0 heading: South! !

!MarsRoverTest methodsFor: 'giros y maniobras' stamp: 'AC 10/10/2019 05:13:51'!
test09GiroAIzquierda

	rover do: 'l' .
	
	self assertRover: rover isInPosition: 0@0 heading: West! !

!MarsRoverTest methodsFor: 'giros y maniobras' stamp: 'AC 10/10/2019 05:13:42'!
test10GiroAIzquierdaConMovimientosYGiros

	rover do: 'lfbbr' .
	
	self assertRover: rover isInPosition: 1@0 heading: North! !


!MarsRoverTest methodsFor: 'helper' stamp: 'AC 10/10/2019 05:08:04'!
assertRover: aRover isInPosition: aPosition heading: aDirection

	self assert: aPosition = aRover position .
	self assert: aDirection = aRover direction .! !


!classDefinition: #MarsRover category: #MarsRover01!
Object subclass: #MarsRover
	instanceVariableNames: 'position direction'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

!MarsRover methodsFor: 'actions' stamp: 'spb 10/5/2019 17:28:31'!
goBackward
	position := position - self stepForward .! !

!MarsRover methodsFor: 'actions' stamp: 'spb 10/5/2019 17:28:16'!
goForward
	position := position + self stepForward .! !

!MarsRover methodsFor: 'actions' stamp: 'AC 10/10/2019 05:05:00'!
stepForward
	^direction stepForward
! !

!MarsRover methodsFor: 'actions' stamp: 'AC 10/10/2019 05:05:18'!
turnLeft
	
	direction := direction directionAfterTurningLeft .! !

!MarsRover methodsFor: 'actions' stamp: 'AC 10/10/2019 05:21:27'!
turnRight

	direction := direction directionAfterTurningRight .! !


!MarsRover methodsFor: 'initialization' stamp: 'spb 10/5/2019 17:35:24'!
atDirection: aDirection
	
	direction := aDirection .! !

!MarsRover methodsFor: 'initialization' stamp: 'spb 10/5/2019 17:35:28'!
atPosition: aPosition

	position := aPosition .! !


!MarsRover methodsFor: 'command' stamp: 'spb 10/9/2019 15:28:52'!
do: aCommandString 
	aCommandString do: [ :command |
		MarsRoverCommand executeCommand: command on: self .
	] .! !

!MarsRover methodsFor: 'command' stamp: 'spb 10/9/2019 15:29:20'!
stop
	self error: self class errorComandoNoValido ! !


!MarsRover methodsFor: 'actitude' stamp: 'spb 10/5/2019 11:09:36'!
direction
	^direction! !

!MarsRover methodsFor: 'actitude' stamp: 'spb 10/5/2019 11:09:20'!
position
	^position! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: #MarsRover01!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'as yet unclassified' stamp: 'spb 10/9/2019 15:20:12'!
errorComandoNoValido
	^'Comando no valido.'! !

!MarsRover class methodsFor: 'as yet unclassified' stamp: 'spb 10/5/2019 11:07:01'!
withPosition: aPosition andDirection: aDirection 
	| rover |
	rover := MarsRover new .
	rover atPosition: aPosition .
	rover atDirection: aDirection .
	^ rover .! !


!classDefinition: #MarsRoverCommand category: #MarsRover01!
Object subclass: #MarsRoverCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverCommand class' category: #MarsRover01!
MarsRoverCommand class
	instanceVariableNames: ''!

!MarsRoverCommand class methodsFor: 'as yet unclassified' stamp: 'AC 10/10/2019 05:12:35'!
canHandle: aCommand

	self subclassResponsibility ! !

!MarsRoverCommand class methodsFor: 'as yet unclassified' stamp: 'spb 10/9/2019 15:27:18'!
executeCommand: aCommand on: aRover

	[
		(self subclasses detect: [:aRoverActionClass | aRoverActionClass canHandle: aCommand ]) executeOn: aRover .
	]
	on: Error
	do: [
		aRover stop .
	].
! !

!MarsRoverCommand class methodsFor: 'as yet unclassified' stamp: 'AC 10/10/2019 05:12:15'!
executeOn: aRover

	self subclassResponsibility ! !


!classDefinition: #MarsRoverCommandGoBackward category: #MarsRover01!
MarsRoverCommand subclass: #MarsRoverCommandGoBackward
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverCommandGoBackward class' category: #MarsRover01!
MarsRoverCommandGoBackward class
	instanceVariableNames: ''!

!MarsRoverCommandGoBackward class methodsFor: 'as yet unclassified' stamp: 'spb 10/5/2019 16:59:59'!
canHandle: aCommand

	^$b = aCommand ! !

!MarsRoverCommandGoBackward class methodsFor: 'as yet unclassified' stamp: 'spb 10/5/2019 17:13:48'!
executeOn: aRover
	
	aRover goBackward .! !


!classDefinition: #MarsRoverCommandGoForward category: #MarsRover01!
MarsRoverCommand subclass: #MarsRoverCommandGoForward
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverCommandGoForward class' category: #MarsRover01!
MarsRoverCommandGoForward class
	instanceVariableNames: ''!

!MarsRoverCommandGoForward class methodsFor: 'as yet unclassified' stamp: 'spb 10/5/2019 17:00:12'!
canHandle: aCommand

	^$f = aCommand ! !

!MarsRoverCommandGoForward class methodsFor: 'as yet unclassified' stamp: 'spb 10/5/2019 17:04:32'!
executeOn: aRover
	
	aRover goForward .! !


!classDefinition: #MarsRoverCommandTurnLeft category: #MarsRover01!
MarsRoverCommand subclass: #MarsRoverCommandTurnLeft
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverCommandTurnLeft class' category: #MarsRover01!
MarsRoverCommandTurnLeft class
	instanceVariableNames: ''!

!MarsRoverCommandTurnLeft class methodsFor: 'as yet unclassified' stamp: 'spb 10/9/2019 15:36:06'!
canHandle: aCommand

	^$l = aCommand ! !

!MarsRoverCommandTurnLeft class methodsFor: 'as yet unclassified' stamp: 'spb 10/9/2019 15:35:51'!
executeOn: aRover
	
	aRover turnLeft .! !


!classDefinition: #MarsRoverCommandTurnRight category: #MarsRover01!
MarsRoverCommand subclass: #MarsRoverCommandTurnRight
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverCommandTurnRight class' category: #MarsRover01!
MarsRoverCommandTurnRight class
	instanceVariableNames: ''!

!MarsRoverCommandTurnRight class methodsFor: 'as yet unclassified' stamp: 'spb 10/5/2019 17:00:21'!
canHandle: aCommand

	^$r = aCommand ! !

!MarsRoverCommandTurnRight class methodsFor: 'as yet unclassified' stamp: 'spb 10/5/2019 17:04:47'!
executeOn: aRover
	
	aRover turnRight .! !


!classDefinition: #MarsRoverDirection category: #MarsRover01!
Object subclass: #MarsRoverDirection
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverDirection class' category: #MarsRover01!
MarsRoverDirection class
	instanceVariableNames: ''!

!MarsRoverDirection class methodsFor: 'movement' stamp: 'AC 10/10/2019 04:57:41'!
stepForward

	self subclassResponsibility! !


!MarsRoverDirection class methodsFor: 'turning' stamp: 'AC 10/10/2019 04:59:42'!
directionAfterTurningLeft

	self subclassResponsibility ! !

!MarsRoverDirection class methodsFor: 'turning' stamp: 'AC 10/10/2019 04:59:54'!
directionAfterTurningRight

	self subclassResponsibility ! !


!classDefinition: #East category: #MarsRover01!
MarsRoverDirection subclass: #East
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'East class' category: #MarsRover01!
East class
	instanceVariableNames: ''!

!East class methodsFor: 'movement' stamp: 'AC 10/10/2019 05:03:17'!
stepForward

	^ 1@0! !


!East class methodsFor: 'turning' stamp: 'AC 10/10/2019 05:13:42'!
directionAfterTurningLeft

	^ North! !

!East class methodsFor: 'turning' stamp: 'AC 10/10/2019 05:13:59'!
directionAfterTurningRight

	^South! !


!classDefinition: #North category: #MarsRover01!
MarsRoverDirection subclass: #North
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'North class' category: #MarsRover01!
North class
	instanceVariableNames: ''!

!North class methodsFor: 'movement' stamp: 'AC 10/10/2019 05:02:55'!
stepForward

	^ 0@1! !


!North class methodsFor: 'turning' stamp: 'AC 10/10/2019 05:13:51'!
directionAfterTurningLeft
	
	^West! !

!North class methodsFor: 'turning' stamp: 'AC 10/10/2019 05:13:14'!
directionAfterTurningRight

	^East! !


!classDefinition: #South category: #MarsRover01!
MarsRoverDirection subclass: #South
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'South class' category: #MarsRover01!
South class
	instanceVariableNames: ''!

!South class methodsFor: 'movement' stamp: 'AC 10/10/2019 05:03:08'!
stepForward
	
	^ 0@-1! !


!South class methodsFor: 'turning' stamp: 'AC 10/10/2019 05:13:14'!
directionAfterTurningLeft

	^East! !

!South class methodsFor: 'turning' stamp: 'AC 10/10/2019 05:13:51'!
directionAfterTurningRight

	^West! !


!classDefinition: #West category: #MarsRover01!
MarsRoverDirection subclass: #West
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover01'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'West class' category: #MarsRover01!
West class
	instanceVariableNames: ''!

!West class methodsFor: 'movement' stamp: 'AC 10/10/2019 05:03:23'!
stepForward
	
	^ -1@0! !


!West class methodsFor: 'turning' stamp: 'AC 10/10/2019 05:13:59'!
directionAfterTurningLeft

	^South! !

!West class methodsFor: 'turning' stamp: 'AC 10/10/2019 05:13:42'!
directionAfterTurningRight
		
	^North! !
