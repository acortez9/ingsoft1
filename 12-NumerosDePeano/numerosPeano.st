!classDefinition: #I category: #numerosPeano!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'numerosPeano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: #numerosPeano!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'nil' stamp: 'as 8/26/2019 18:25:40'!
next
	^II
	! !


!I class methodsFor: 'as yet unclassified' stamp: 'as 8/26/2019 19:19:13'!
* numeroAMultiplicar
	^numeroAMultiplicar ! !

!I class methodsFor: 'as yet unclassified' stamp: 'as 8/26/2019 19:02:13'!
+ numeroASumar

	^numeroASumar  next! !

!I class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 01:22:23'!
- numeroARestar
	^self error: self descripcionDeErrorDeNumerosNegativosNoSoportados ! !

!I class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 01:23:59'!
/ denominador
	
	denominador = I
		ifTrue: [^I.]
		ifFalse: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor .].! !

!I class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 01:19:32'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'No se puede dividir por un numero mayor'! !

!I class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 01:19:03'!
descripcionDeErrorDeNumerosNegativosNoSoportados

	^'Numeros negativos no soportados'! !

!I class methodsFor: 'as yet unclassified' stamp: 'as 8/26/2019 18:23:00'!
previous

	! !

!I class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 01:22:02'!
restaDevuelveFalseSiEsMenor
	^false! !

!I class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 01:38:55'!
restaDevuelveFalseSiEsMenor: numeroARestar
	^false! !

!I class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:00:53'!
restaInversa: numeroARestar
	^numeroARestar previous.! !


!classDefinition: #II category: #numerosPeano!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'numerosPeano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: #numerosPeano!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'as yet unclassified' stamp: 'as 8/26/2019 19:20:27'!
* numeroAMultiplicar
	^numeroAMultiplicar  + (self previous * numeroAMultiplicar)! !

!II class methodsFor: 'as yet unclassified' stamp: 'as 8/26/2019 19:03:03'!
+ numeroASumar
	
	^self previous + numeroASumar next.! !

!II class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:03:47'!
- numeroARestar
	^numeroARestar restaInversa: self.
	! !

!II class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 01:45:31'!
/ denominador
	
	|resta division|
	resta := self restaDevuelveFalseSiEsMenor: denominador.
	self = denominador ifTrue: [^I].
	resta = false ifTrue: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor .].
	
	division := resta / denominador.
	division = false  ifTrue: [^I]
		ifFalse: [^I + division].! !

!II class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 01:25:53'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'No se puede dividir por un numero mayor'! !

!II class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 01:32:20'!
descripcionDeErrorDeNumerosNegativosNoSoportados

	^'Numeros negativos no soportados'! !

!II class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:07:23'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !

!II class methodsFor: 'as yet unclassified' stamp: 'as 8/26/2019 19:06:21'!
next
	next ifNil: [
	next := self cloneNamed: self name, 'I'.
	next previous: self.
	].
	^next! !

!II class methodsFor: 'as yet unclassified' stamp: 'as 8/26/2019 19:15:22'!
previous
	^previous! !

!II class methodsFor: 'as yet unclassified' stamp: 'as 8/26/2019 19:04:54'!
previous: anterior
	
	previous := anterior! !

!II class methodsFor: 'as yet unclassified' stamp: 'as 8/26/2019 19:16:18'!
removeAllNext


	next ifNotNil:

	[

		next removeAllNext.

		next removeFromSystem.

		next := nil.

	]! !

!II class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 01:21:00'!
restaDevuelveFalseSiEsMenor: numeroARestar
	
	|resultado|
	
	numeroARestar = I
		ifFalse: [resultado := self previous restaDevuelveFalseSiEsMenor: numeroARestar previous.]
		ifTrue: [resultado := self previous.].
		
	^resultado! !

!II class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:03:06'!
restaInversa: numeroARestar
	
	^self previous restaInversa: numeroARestar previous ! !


!classDefinition: #III category: #numerosPeano!
DenotativeObject subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'numerosPeano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: #numerosPeano!
III class
	instanceVariableNames: 'next previous'!

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
* numeroAMultiplicar
	^numeroAMultiplicar  + (self previous * numeroAMultiplicar)! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
+ numeroASumar
	
	^self previous + numeroASumar next.! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
- numeroARestar
	^numeroARestar restaInversa: self.
	! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
/ denominador
	
	|resta division|
	resta := self restaDevuelveFalseSiEsMenor: denominador.
	self = denominador ifTrue: [^I].
	resta = false ifTrue: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor .].
	
	division := resta / denominador.
	division = false  ifTrue: [^I]
		ifFalse: [^I + division].! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'No se puede dividir por un numero mayor'! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
descripcionDeErrorDeNumerosNegativosNoSoportados

	^'Numeros negativos no soportados'! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:07:23'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIII.
	previous := II.! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
next
	next ifNil: [
	next := self cloneNamed: self name, 'I'.
	next previous: self.
	].
	^next! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
previous
	^previous! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
previous: anterior
	
	previous := anterior! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
removeAllNext


	next ifNotNil:

	[

		next removeAllNext.

		next removeFromSystem.

		next := nil.

	]! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
restaDevuelveFalseSiEsMenor: numeroARestar
	
	|resultado|
	
	numeroARestar = I
		ifFalse: [resultado := self previous restaDevuelveFalseSiEsMenor: numeroARestar previous.]
		ifTrue: [resultado := self previous.].
		
	^resultado! !

!III class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
restaInversa: numeroARestar
	
	^self previous restaInversa: numeroARestar previous ! !


!classDefinition: #IIII category: #numerosPeano!
DenotativeObject subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'numerosPeano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: #numerosPeano!
IIII class
	instanceVariableNames: 'next previous'!

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
* numeroAMultiplicar
	^numeroAMultiplicar  + (self previous * numeroAMultiplicar)! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
+ numeroASumar
	
	^self previous + numeroASumar next.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
- numeroARestar
	^numeroARestar restaInversa: self.
	! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
/ denominador
	
	|resta division|
	resta := self restaDevuelveFalseSiEsMenor: denominador.
	self = denominador ifTrue: [^I].
	resta = false ifTrue: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor .].
	
	division := resta / denominador.
	division = false  ifTrue: [^I]
		ifFalse: [^I + division].! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'No se puede dividir por un numero mayor'! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
descripcionDeErrorDeNumerosNegativosNoSoportados

	^'Numeros negativos no soportados'! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:07:23'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := III.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
next
	next ifNil: [
	next := self cloneNamed: self name, 'I'.
	next previous: self.
	].
	^next! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
previous
	^previous! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
previous: anterior
	
	previous := anterior! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
removeAllNext


	next ifNotNil:

	[

		next removeAllNext.

		next removeFromSystem.

		next := nil.

	]! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
restaDevuelveFalseSiEsMenor: numeroARestar
	
	|resultado|
	
	numeroARestar = I
		ifFalse: [resultado := self previous restaDevuelveFalseSiEsMenor: numeroARestar previous.]
		ifTrue: [resultado := self previous.].
		
	^resultado! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'ssda 9/2/2019 02:04:00'!
restaInversa: numeroARestar
	
	^self previous restaInversa: numeroARestar previous ! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!