#ifndef BRAZO_DE_ROVER_CPP
#define BRAZO_DE_ROVER_CPP

#include "brazo_de_rover.h"


brazo_de_rover::brazo_de_rover() {
	_pinza_abierta = true;
	_velocidad_mecha = 0;
	_mecha_girando = false;
	_direccion_mecha = SENTIDO_RELOJ;
}

void brazo_de_rover::analizar_suelo_y_tomar_muestra(int dureza, int porosidad) {
	tomar_muestra(identificar_suelo(dureza,porosidad));
}

tipo_de_suelo brazo_de_rover::identificar_suelo(int dureza, int porosidad) {
	if (dureza >= 75 && porosidad < 25) return PIEDRA;
	else if (dureza < 25 && porosidad >= 75) return POLVO;
	else if (dureza >= 25 && dureza < 75 && porosidad < 75 && porosidad >= 25) return INTERMEDIO;
	else return DESCONOCIDO;
}

void brazo_de_rover::rotar_mecha() {
	_mecha_girando = true;
}

void brazo_de_rover::detener_mecha() {
	_mecha_girando = false;
}

void brazo_de_rover::abrir_pinza() {
	_pinza_abierta = true;
}

void brazo_de_rover::cerrar_pinza() {
	_pinza_abierta = false;
}

void brazo_de_rover::operar_mecha(direccion_mecha direccion, int velocidad, int tiempo_segundos) {
	_velocidad_mecha = velocidad;
	_direccion_mecha = direccion;
	rotar_mecha();
	sleep(tiempo_segundos);
	detener_mecha();
}

void brazo_de_rover::tomar_muestra (tipo_de_suelo suelo){
	
	switch(suelo) {
		case PIEDRA:
			abrir_pinza();
			operar_mecha(SENTIDO_RELOJ,150,10*60);
			cerrar_pinza();
			operar_mecha(SENTIDO_CONTRARIO_RELOJ,150,10*60);
			break;
		case POLVO:
			abrir_pinza();
			operar_mecha(SENTIDO_CONTRARIO_RELOJ,100,5*60);
			cerrar_pinza();
			operar_mecha(SENTIDO_RELOJ,150,5*60);
			break;
		case INTERMEDIO:
			abrir_pinza();
			operar_mecha(SENTIDO_RELOJ,150,5*60);
			cerrar_pinza();
			operar_mecha(SENTIDO_CONTRARIO_RELOJ,100,10*60);
			break;
		case DESCONOCIDO:
			break;
		default:
			break;
	}
	
}

#endif //BRAZO_DE_ROVER_CPP