#ifndef BRAZO_DE_ROVER_H
#define BRAZO_DE_ROVER_H

#define DESCONOCIDO 0
#define PIEDRA 1
#define POLVO 2
#define POLVO 3

#define SENTIDO_RELOJ 0
#define SENTIDO_CONTRARIO_RELOJ 1

typedef int tipo_de_suelo;
typedef int direccion_rotacion;

class brazo_de_rover {
	
	public:
	
	brazo_de_rover();
	void analizar_suelo_y_tomar_muestra(int dureza, int porosidad);
	
	private:
	
	tipo_de_suelo identificar_suelo(int dureza, int porosidad);
	
	bool _pinza_abierta;
	int _velocidad_mecha;
	bool _mecha_girando;
	direccion_rotacion _direccion_mecha;
	
	void rotar_mecha();
	void detener_mecha();
	
	void abrir_pinza();
	void cerrar_pinza();
	
	void operar_mecha(direccion_rotacion direccion, int velocidad, int tiempo_segundos);
	void tomar_muestra (tipo_de_suelo suelo);
}


#endif //BRAZO_DE_ROVER_H
