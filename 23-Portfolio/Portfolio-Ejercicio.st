!classDefinition: #PortfolioTest category: #'Portfolio-Ejercicio'!
TestCase subclass: #PortfolioTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/20/2019 07:25:27'!
test01newPortfolioHas0Balance

	|portfolio|
	
	portfolio := Portfolio new.
	
	self assert: portfolio balance = 0! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/20/2019 07:38:13'!
test02PortfolioCanRecognizeHolderDirectlyAddedToIt

	| portfolio holder1 holder2 dummyHolder1 dummyHolder2 |	
	
	portfolio := Portfolio new.
	
	holder1 := Portfolio new.
	
	dummyHolder1 := Portfolio new.
	
	holder2 := ReceptiveAccount new.
	
	dummyHolder2 := ReceptiveAccount new.
	
	portfolio addHolder: holder1.
	
	portfolio addHolder: holder2.
	
	self assert: (portfolio hasHolder: holder1).
	
	self assert: (portfolio hasHolder: holder2).
	
	self deny: (portfolio hasHolder: dummyHolder1).
	
	self deny: (portfolio hasHolder: dummyHolder2)! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/20/2019 07:41:29'!
test03PortfolioCanRecognizeHolderAddedBelowToIt

	| portfolio holder1 holder2 dummyHolder1 dummyHolder2 |	
	
	portfolio := Portfolio new.
	
	holder1 := Portfolio new.
	
	dummyHolder1 := Portfolio new.
	
	holder2 := ReceptiveAccount new.
	
	dummyHolder2 := ReceptiveAccount new.
	
	holder1 addHolder: holder2.
	
	portfolio addHolder: holder1.
	
	self assert: (portfolio hasHolder: holder1).
	
	self assert: (portfolio hasHolder: holder2).
	
	self deny: (portfolio hasHolder: dummyHolder1).
	
	self deny: (portfolio hasHolder: dummyHolder2)! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/20/2019 07:47:22'!
test04PortfolioKnowsIfItsOnAnotherPortfolio

	| bigPortfolio smallPortfolio dummyPortfolio |	
	
	bigPortfolio := Portfolio new.
	
	smallPortfolio := Portfolio new.
	
	dummyPortfolio := Portfolio new.
	
	bigPortfolio addHolder: smallPortfolio.
	
	self assert: (smallPortfolio isIn: bigPortfolio).
	
	self deny: (smallPortfolio isIn: dummyPortfolio).! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/20/2019 07:56:20'!
test05CantAddHolderIfItRepeatsInformation

	| bigPortfolio smallPortfolio account1 |
	
	bigPortfolio  := Portfolio new.
	
	smallPortfolio  := Portfolio new.
	
	account1  := ReceptiveAccount new.
	
	bigPortfolio addHolder: account1.
	
	smallPortfolio addHolder: account1.
	
	self should: [
		bigPortfolio addHolder: smallPortfolio.
	] 
	raise: Error
	withExceptionDo: [
		:anError |
		self assert: Portfolio errorMessageRepeatsInformation = anError messageText .
	] 
	
	
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/21/2019 02:56:02'!
test06CantAddHolderIfItRepeatsInformationComplex

	| biggerPortfolio bigPortfolio1 bigPortfolio2 mediumPortfolio smallPortfolio account1 |
	
	biggerPortfolio  := Portfolio new.
	bigPortfolio1  := Portfolio new. 
	bigPortfolio2  := Portfolio new. 
	
	mediumPortfolio  := Portfolio new.
	
	smallPortfolio  := Portfolio new.
	
	account1  := ReceptiveAccount new.
	
	biggerPortfolio addHolder: bigPortfolio1.
	
	biggerPortfolio addHolder: bigPortfolio2 .
	
	bigPortfolio2 addHolder: account1 .
	
	bigPortfolio1 addHolder: mediumPortfolio .
	
	smallPortfolio addHolder: account1.
	
	self should: [
		mediumPortfolio addHolder: smallPortfolio.
	] 
	raise: Error
	withExceptionDo: [
		:anError |
		self assert: Portfolio errorMessageRepeatsInformation = anError messageText .
	] 
	
	
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/21/2019 02:56:13'!
test07CanAddHolderEvenIfTheyEndUpInSameGraphButWithNoSharedInformation

	"este test fallaba con la primera implementacion que hicimos"

	| bigPortfolioA bigPortfolioB smallPortfolioC smallPortfolioD |
	
	bigPortfolioA := Portfolio new.
	bigPortfolioB := Portfolio new.
	smallPortfolioC := Portfolio new.
	smallPortfolioD := Portfolio new.
	
	bigPortfolioA addHolder: smallPortfolioC.
	bigPortfolioA addHolder: smallPortfolioD.
	bigPortfolioB addHolder: smallPortfolioC.
	bigPortfolioB addHolder: smallPortfolioD.
	
	
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/21/2019 02:58:58'!
test08CantAddPortfolioToItself

	| portfolio |
	
	portfolio  := Portfolio new.
	
	
	self should: [
		portfolio addHolder: portfolio.
	] 
	raise: Error
	withExceptionDo: [
		:anError |
		self assert: Portfolio errorMessageRepeatsInformation = anError messageText .
	] 
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/21/2019 03:01:57'!
test09balanceSumsBalancesOfAllAccountsInPortfolio

	|portfolio account1 account2 account3|
	
	portfolio := Portfolio new.
	account1 := ReceptiveAccount new.
	account2 := ReceptiveAccount new.
	account3 := ReceptiveAccount new.
	
	portfolio addHolder: account1.
	portfolio addHolder: account2.
	portfolio addHolder: account3.
	
	Deposit register: 40 on: account1.
	Withdraw register: 20 on: account1.
	Deposit register: 70 on: account1.
	Withdraw register: 30 on: account1.
	Deposit register: 80 on: account1.
	Deposit register: 40 on: account1.
	
	self assert: portfolio balance = 180! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/21/2019 03:02:02'!
test10balanceSumsBalancesOfAllAccountsAndPortfoliosInPortfolio

	|bigPortfolio smallPortfolio account1 account2 account3|
	
	bigPortfolio := Portfolio new.
	smallPortfolio := Portfolio new.
	account1 := ReceptiveAccount new.
	account2 := ReceptiveAccount new.
	account3 := ReceptiveAccount new.
	
	bigPortfolio addHolder: account1.
	bigPortfolio addHolder: account2.
	bigPortfolio addHolder: smallPortfolio.
	smallPortfolio addHolder: account3.
	
	Deposit register: 40 on: account1.
	Withdraw register: 20 on: account1.
	Deposit register: 70 on: account1.
	Withdraw register: 30 on: account1.
	Deposit register: 80 on: account1.
	Deposit register: 40 on: account1.
	
	self assert: bigPortfolio balance = 180! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/21/2019 03:02:06'!
test11PortfolioCanRecognizeTransactionsMadeByHolders

	|bigPortfolio smallPortfolio transaction1 transaction2 dummyTransaction account1 account2|
	
	bigPortfolio := Portfolio new.
	smallPortfolio := Portfolio new.
	
	bigPortfolio addHolder: smallPortfolio .
	
	account1 := ReceptiveAccount new.
	account2 := ReceptiveAccount new.
	
	transaction1 := Deposit for: 1.
	transaction2 := Deposit for: 2.
	dummyTransaction := Deposit for: 3.
	
	account1 register: transaction1.
	account2 register: transaction2.
	
	bigPortfolio addHolder: account1 .
	smallPortfolio addHolder: account2 .
	
	self assert: (bigPortfolio hasRegistered: transaction1 ).
	self assert: (bigPortfolio hasRegistered: transaction2 ).
	self deny: (bigPortfolio hasRegistered: dummyTransaction )
	
	
	! !

!PortfolioTest methodsFor: 'tests' stamp: 'AC 10/21/2019 03:02:10'!
test12PortfolioKnowsItsTransactions

	|bigPortfolio smallPortfolio transaction1 transaction2 dummyTransaction account1 account2 transactions|
	
	bigPortfolio := Portfolio new.
	smallPortfolio := Portfolio new.
	
	bigPortfolio addHolder: smallPortfolio .
	
	account1 := ReceptiveAccount new.
	account2 := ReceptiveAccount new.
	
	transaction1 := Deposit for: 1.
	transaction2 := Deposit for: 2.
	dummyTransaction := Deposit for: 3.
	
	account1 register: transaction1.
	account2 register: transaction2.
	
	bigPortfolio addHolder: account1 .
	smallPortfolio addHolder: account2 .
	
	transactions := bigPortfolio transactions .
	
	self assert: transactions size = 2.
	self assert: (transactions includes: transaction1) .
	self assert: (transactions includes: transaction2) .
	self deny: (transactions includes: dummyTransaction )
	
	
	! !


!classDefinition: #ReceptiveAccountTest category: #'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:19:48'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:19:54'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:02'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:32'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:46'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:54'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 100.
	withdraw := Withdraw for: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:21:24'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 100 on: account1.
		
	self assert: 1 equals: account1 transactions size .
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #AccountTransaction category: #'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'AC 10/20/2019 07:16:23'!
applyToBalance: aBalance

	self subclassResponsibility! !

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: #'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/17/2019 03:22:00'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: #'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'AC 10/20/2019 07:18:00'!
applyToBalance: aBalance

	^aBalance + value
	
	! !

!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: #'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: #'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'AC 10/20/2019 07:06:39'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'AC 10/20/2019 07:17:53'!
applyToBalance: aBalance

	^aBalance - value! !

!Withdraw methodsFor: 'value' stamp: 'AC 10/20/2019 07:09:40'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: #'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #TransactionHolder category: #'Portfolio-Ejercicio'!
Object subclass: #TransactionHolder
	instanceVariableNames: 'portfoliosImIn'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!TransactionHolder methodsFor: 'initialization' stamp: 'AC 10/20/2019 07:20:03'!
initialize

	transactions := OrderedCollection new.! !


!TransactionHolder methodsFor: 'transactions' stamp: 'AC 10/20/2019 07:20:03'!
register: aTransaction

	transactions add: aTransaction 
! !

!TransactionHolder methodsFor: 'transactions' stamp: 'AC 10/20/2019 07:20:03'!
transactions 

	^ transactions copy! !


!TransactionHolder methodsFor: 'balance' stamp: 'AC 10/20/2019 07:20:03'!
balance
	|result|
	result := 0.	
	transactions do: [ :aTransaction | result := aTransaction applyToBalance: result.].
	
	^result! !


!TransactionHolder methodsFor: 'testing' stamp: 'AC 10/20/2019 07:20:03'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !


!TransactionHolder methodsFor: 'observers' stamp: 'AC 10/20/2019 08:12:39'!
allNodesExceptParent: parent

	self subclassResponsibility ! !

!TransactionHolder methodsFor: 'observers' stamp: 'AC 10/20/2019 07:43:57'!
hasHolder: aHolder
	
	self subclassResponsibility! !


!classDefinition: #Portfolio category: #'Portfolio-Ejercicio'!
TransactionHolder subclass: #Portfolio
	instanceVariableNames: 'holders'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'initialization' stamp: 'AC 10/20/2019 08:54:52'!
initialize

	holders := Set new.
	portfoliosImIn := Set new.! !


!Portfolio methodsFor: 'transactions' stamp: 'AC 10/20/2019 08:56:54'!
transactions 
	| result |
	
	result := Set new.
	
	holders do: [:aHolder | result := (result union: (aHolder transactions))].
	
	^result! !


!Portfolio methodsFor: 'balance' stamp: 'AC 10/20/2019 08:36:31'!
balance
	|result|
	result := 0.	
	
	holders do: [:aHolder | result := result + aHolder balance].
	
	^result! !


!Portfolio methodsFor: 'testing' stamp: 'AC 10/20/2019 07:43:40'!
hasHolder: aHolder

	^ (holders anySatisfy: [ :anyHolder | anyHolder hasHolder: aHolder]) or: (holders includes: aHolder)! !

!Portfolio methodsFor: 'testing' stamp: 'AC 10/20/2019 08:43:05'!
hasRegistered: aTransaction

	^ holders anySatisfy: [:aHolder | aHolder hasRegistered: aTransaction ]
! !

!Portfolio methodsFor: 'testing' stamp: 'AC 10/20/2019 07:49:52'!
isIn: aPortfolio

	^portfoliosImIn includes: aPortfolio ! !


!Portfolio methodsFor: 'modifiers' stamp: 'AC 10/20/2019 08:18:03'!
addHolder: aHolder

	(self addingHolderWouldRepeatInfo: aHolder) ifTrue: [self error: Portfolio errorMessageRepeatsInformation ].

	holders add: aHolder.
	
	aHolder addToPortfolio: self! !

!Portfolio methodsFor: 'modifiers' stamp: 'AC 10/20/2019 07:48:19'!
addToPortfolio: aBigPortfolio

	portfoliosImIn add: aBigPortfolio ! !


!Portfolio methodsFor: 'repeats info' stamp: 'AC 10/21/2019 02:47:08'!
addingHolderWouldRepeatInfo: aHolder

	|allNodesInSelf  allDownwardNodesInPotentialHolder|

	allNodesInSelf := self allNodes.
	
	allDownwardNodesInPotentialHolder  := aHolder allNodesDownwards.
	
	^ allNodesInSelf anySatisfy: [:aNode | allDownwardNodesInPotentialHolder includes: aNode]! !

!Portfolio methodsFor: 'repeats info' stamp: 'AC 10/21/2019 03:00:30'!
allNodes

	|nodes|
	
	nodes := Set new.
	
	holders do: [:aHolder | nodes := nodes union: (aHolder allNodesDownwards )].
	
	portfoliosImIn do: [:aPortfolio | nodes := nodes union: (aPortfolio allNodesExceptChild: self)].
	
	nodes add: self.
	
	^nodes! !

!Portfolio methodsFor: 'repeats info' stamp: 'AC 10/21/2019 03:00:11'!
allNodesDownwards

	|nodes|
	
	nodes := Set new.
	
	holders do: [:aHolder | nodes := nodes union: (aHolder allNodesDownwards )].
	
	nodes add: self.
	
	^nodes! !

!Portfolio methodsFor: 'repeats info' stamp: 'AC 10/21/2019 03:00:01'!
allNodesExceptChild: child

	|nodes|
	
	nodes := Set new.
	
	holders do: [:aHolder | 
		aHolder = child ifFalse: [
			nodes add: aHolder. nodes := nodes union: (aHolder allNodesDownwards
			)]
		].
	
	portfoliosImIn do: [:aPortfolio | nodes add: aPortfolio. nodes := nodes union: (aPortfolio allNodesExceptChild: self)].
	
	nodes add: self.
	
	^nodes! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: #'Portfolio-Ejercicio'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'errors' stamp: 'AC 10/20/2019 07:56:50'!
errorMessageRepeatsInformation

	^ 'Adding this portfolio would cause repeated information'! !


!classDefinition: #ReceptiveAccount category: #'Portfolio-Ejercicio'!
TransactionHolder subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'AC 10/20/2019 08:54:39'!
initialize

	transactions := Set new.
	portfoliosImIn := Set new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'AC 10/20/2019 07:14:36'!
balance
	|result|
	result := 0.	
	transactions do: [ :aTransaction | result := aTransaction applyToBalance: result.].
	
	^result! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'AC 10/20/2019 07:44:09'!
hasHolder: aHolder

	^false! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/17/2019 03:28:43'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'AC 10/20/2019 08:21:27'!
isIn: aPortfolio

	^ portfoliosImIn includes: aPortfolio 

	! !


!ReceptiveAccount methodsFor: 'repeats info' stamp: 'AC 10/21/2019 03:01:26'!
allNodesDownwards

	|nodes|
	
	nodes := Set new.
	
	nodes add: self.
	
	^nodes! !


!ReceptiveAccount methodsFor: 'modifiers' stamp: 'AC 10/20/2019 08:21:55'!
addToPortfolio: aBigPortfolio

	portfoliosImIn add: aBigPortfolio ! !
