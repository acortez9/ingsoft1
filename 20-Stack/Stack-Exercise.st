!classDefinition: #OOStackTest category: #'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'Something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:31'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'Something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/8/2012 08:20'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'Something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:33'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'First'.
	secondPushedObject := 'Second'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:35'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'Something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:36'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'Something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'AC 9/18/2019 18:51:55'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:36'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'Something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: #'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 19:59:25'!
test01emptyStackReturnsEmptyStack

	|stack result finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	result := finder findSentencesInStack: stack withPrefix: 'random'.
	
	self assert: result isEmpty! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 20:23:57'!
test02prefixExaminatorReturnsTrue

	|stack finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'qwertyuiop'.
	
	self assert: (finder checkSentence: stack top forPrefix: 'qwer')! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 20:26:49'!
test03prefixExaminatorReturnsFalse

	|stack finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'qwertyuiop'.
	
	self deny: (finder checkSentence: stack top forPrefix: 'qwop')! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 20:27:24'!
test04prefixExaminatorIsCaseSensitive

	|stack finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'qwertyuiop'.
	
	self deny: (finder checkSentence: stack top forPrefix: 'Qwer')! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 20:37:43'!
test05prefixExaminatorReturnsTrueForEqualSentenceAndPrefix

	|stack finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'qwertyuiop'.
	
	self assert: (finder checkSentence: stack top forPrefix: 'qwertyuiop')! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 20:37:21'!
test06noValidSentencesReturnsEmptyStack

	|stack result finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'Random' .
	stack push: 'rAndom' .
	stack push: 'modnar' .
	stack push: 'xxxxxxx' .
	
	result := finder findSentencesInStack: stack withPrefix: 'random'.
	
	self assert: result isEmpty! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 20:48:06'!
test07copyStackReturnsSameStack

	|stack newStack finder|
	
	stack := OOStack new.
	newStack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'ran' .
	stack push: 'random' .
	stack push: 'randomize' .
	stack push: 'randomization' .
	
	newStack := finder copyStack: stack.
	
	self assert: (newStack pop = 'randomization').	
	self assert: (newStack pop = 'randomize').	
	self assert: (newStack pop = 'random').	
	self assert: (newStack pop = 'ran').
	self assert: newStack isEmpty! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 20:49:08'!
test08copyDoesNotAffectOriginalStack

	|stack finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'ran' .
	stack push: 'random' .
	stack push: 'randomize' .
	stack push: 'randomization' .
	
	finder copyStack: stack.
	
	self assert: (stack pop = 'randomization').	
	self assert: (stack pop = 'randomize').	
	self assert: (stack pop = 'random').	
	self assert: (stack pop = 'ran').
	self assert: stack isEmpty! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 20:49:44'!
test09onlyValidSentencesReturnsStackOfSameSize

	|stack result finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'ran' .
	stack push: 'random' .
	stack push: 'randomize' .
	stack push: 'randomization' .
	
	result := finder findSentencesInStack: stack withPrefix: 'ran'.
	
	self assert: (result size = stack size)! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 20:51:34'!
test10findingSentencesDoesNotAffectOriginalStack

	|stack finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'ran' .
	stack push: 'random' .
	stack push: 'randomize' .
	stack push: 'randomization' .
	
	finder findSentencesInStack: stack withPrefix: 'ran'.
	
	self assert: (stack pop = 'randomization').	
	self assert: (stack pop = 'randomize').	
	self assert: (stack pop = 'random').	
	self assert: (stack pop = 'ran').
	self assert: stack isEmpty! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 21:02:25'!
test11appearsWorksWell

	|stack finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'ran' .
	stack push: 'random' .
	stack push: 'randomize' .
	stack push: 'randomization' .

	self assert: (finder appears: 'random' inStack: stack).
	self deny: (finder appears: 'qwerty' inStack: stack)! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/18/2019 21:03:28'!
test12resultingStackHasOnlyValidSentences

	|stack result finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	stack push: 'ran' .
	stack push: 'qwerty' .
	stack push: 'randomize' .
	stack push: 'RANDOM' .
	
	result := finder findSentencesInStack: stack withPrefix: 'ran'.
	
	self assert: (finder appears: 'ran' inStack: result).
	self assert: (finder appears: 'randomize' inStack: result).
	self assert: (result size = 2).! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/19/2019 14:07:10'!
test13emptyPrefixRaisesError

	|stack finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	self
		should: [ finder findSentencesInStack: stack withPrefix: '' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix emptyPrefixErrorDescription ]! !

!SentenceFinderByPrefixTest methodsFor: 'tests' stamp: 'AC 9/19/2019 14:08:02'!
test14emptySpaceInPrefixRaisesError

	|stack finder|
	
	stack := OOStack new.
	finder := SentenceFinderByPrefix new.
	
	self
		should: [ finder findSentencesInStack: stack withPrefix: 'Im a naughty prefix' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix emptySpaceInPrefixErrorDescription ]! !


!classDefinition: #OOStack category: #'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'stackContent'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'status' stamp: 'AC 9/18/2019 18:34:24'!
isEmpty

	^stackContent isEmpty! !

!OOStack methodsFor: 'status' stamp: 'AC 9/18/2019 18:47:34'!
size

	^stackContent size! !

!OOStack methodsFor: 'status' stamp: 'AC 9/18/2019 18:34:36'!
top

	^stackContent top! !


!OOStack methodsFor: 'modifiers' stamp: 'AC 9/18/2019 18:36:10'!
pop

	| lastObject |
	
	lastObject := stackContent top.
	
	stackContent := stackContent removeContent.
	
	^lastObject! !

!OOStack methodsFor: 'modifiers' stamp: 'AC 9/18/2019 18:35:20'!
push: anObject

	stackContent := stackContent addContent: anObject
	! !


!OOStack methodsFor: 'initialization' stamp: 'AC 9/18/2019 18:40:52'!
initialize

	stackContent := EmptyStackContent new! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: #'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'HernanWilkinson 5/7/2012 11:51'!
stackEmptyErrorDescription
	
	^ 'Stack is empty'! !


!classDefinition: #SentenceFinderByPrefix category: #'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'finders' stamp: 'AC 9/19/2019 14:02:47'!
findSentencesInStack: stackWithSentences withPrefix: prefix

	| imputStack resultStack |
	
	self checkIfValid: prefix.
	
	imputStack := self copyStack: stackWithSentences.
	
	resultStack := OOStack new.
	
	[imputStack isEmpty] whileFalse: [
		(self checkSentence: imputStack top forPrefix: prefix) ifTrue: [
			resultStack push: imputStack top.
		].
		imputStack pop.
	].

	^resultStack! !


!SentenceFinderByPrefix methodsFor: 'examinators' stamp: 'AC 9/19/2019 14:04:21'!
checkIfValid: prefix

	|iterator|
	
	prefix isEmpty ifTrue: [self error: SentenceFinderByPrefix emptyPrefixErrorDescription. ].
	
	iterator := 1.
	
	[iterator <= prefix size] whileTrue: [
		(prefix at: iterator) = ($ ) ifTrue: [self error: SentenceFinderByPrefix emptySpaceInPrefixErrorDescription .].
		iterator := iterator + 1.
		].! !

!SentenceFinderByPrefix methodsFor: 'examinators' stamp: 'AC 9/18/2019 20:26:19'!
checkSentence: sentence forPrefix: prefix

	|iterator|
	
	iterator := 1.
	
	[(iterator <= sentence size) and: (iterator <= prefix size)] whileTrue: [
		(sentence at: iterator) = (prefix at: iterator) ifFalse: [^false].
		iterator := iterator + 1.
	].

	iterator = (prefix size + 1) ifTrue: [^true].
	^false! !


!SentenceFinderByPrefix methodsFor: 'helpers' stamp: 'AC 9/18/2019 21:00:34'!
appears: anObject inStack: aStack

	|newStack|
	
	newStack := self copyStack: aStack.
	
	[newStack isEmpty] whileFalse: [
			anObject = newStack pop ifTrue: [^true].
		].
	
	^false! !

!SentenceFinderByPrefix methodsFor: 'helpers' stamp: 'AC 9/19/2019 13:53:50'!
copyStack: aStackToCopy

	| reversedStack1 reversedStack2 copiedStack |
	
	reversedStack2 := OOStack new.
	reversedStack1 := OOStack new.
	
	[aStackToCopy isEmpty] whileFalse: [reversedStack2 push: aStackToCopy top. reversedStack1 push: aStackToCopy pop. ].
	
	copiedStack := OOStack new.
	
	[reversedStack2 isEmpty] whileFalse: [copiedStack push: reversedStack2 pop. aStackToCopy push: reversedStack1 pop.].
	
	^copiedStack! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: #'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'errors' stamp: 'AC 9/19/2019 13:58:47'!
emptyPrefixErrorDescription

	^ 'Prefix is empty'! !

!SentenceFinderByPrefix class methodsFor: 'errors' stamp: 'AC 9/19/2019 13:59:15'!
emptySpaceInPrefixErrorDescription

	^ 'Prefix has an empty space'! !


!classDefinition: #StackContent category: #'Stack-Exercise'!
Object subclass: #StackContent
	instanceVariableNames: 'content'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackContent methodsFor: 'modifiers' stamp: 'AC 9/18/2019 18:25:59'!
addContent: anObject

	content addLast: anObject.
	
	^StackContent initializeWith: content! !

!StackContent methodsFor: 'modifiers' stamp: 'AC 9/18/2019 18:18:33'!
content: aCollection

	content := aCollection! !

!StackContent methodsFor: 'modifiers' stamp: 'AC 9/18/2019 18:30:36'!
removeContent

	self subclassResponsibility! !


!StackContent methodsFor: 'status' stamp: 'AC 9/18/2019 18:22:40'!
isEmpty

	self subclassResponsibility! !

!StackContent methodsFor: 'status' stamp: 'AC 9/18/2019 18:47:43'!
size

	^content size! !

!StackContent methodsFor: 'status' stamp: 'AC 9/18/2019 18:30:30'!
top

	self subclassResponsibility! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StackContent class' category: #'Stack-Exercise'!
StackContent class
	instanceVariableNames: ''!

!StackContent class methodsFor: 'initialization' stamp: 'AC 9/18/2019 18:46:06'!
initializeWith: aCollection

	| resultingClass resultStackContent |
	
	resultingClass := EmptyStackContent.
	
	aCollection select: [ :dummy | resultingClass := NonEmptyStackContent. false ].
	
	resultStackContent := resultingClass new.
	
	resultStackContent content: aCollection.
	
	^resultStackContent 
	! !


!classDefinition: #EmptyStackContent category: #'Stack-Exercise'!
StackContent subclass: #EmptyStackContent
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!EmptyStackContent methodsFor: 'status' stamp: 'AC 9/18/2019 18:22:56'!
isEmpty

	^true! !

!EmptyStackContent methodsFor: 'status' stamp: 'AC 9/18/2019 18:52:32'!
top

	self error:  OOStack stackEmptyErrorDescription ! !


!EmptyStackContent methodsFor: 'modifiers' stamp: 'AC 9/18/2019 18:52:28'!
removeContent

	self error:  OOStack stackEmptyErrorDescription ! !


!EmptyStackContent methodsFor: 'initialization' stamp: 'AC 9/18/2019 18:40:27'!
initialize

	content := OrderedCollection new! !


!classDefinition: #NonEmptyStackContent category: #'Stack-Exercise'!
StackContent subclass: #NonEmptyStackContent
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!NonEmptyStackContent methodsFor: 'status' stamp: 'AC 9/18/2019 18:23:05'!
isEmpty

	^false! !

!NonEmptyStackContent methodsFor: 'status' stamp: 'AC 9/18/2019 18:33:41'!
top

	^content at: content size! !


!NonEmptyStackContent methodsFor: 'modifiers' stamp: 'AC 9/18/2019 18:29:51'!
removeContent

	content removeLast.
	
	^ StackContent initializeWith: content
	
	! !
