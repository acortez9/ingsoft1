!classDefinition: #TerniLapilliTest category: #TerniLapilli!
TestCase subclass: #TerniLapilliTest
	instanceVariableNames: 'tateti'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilliTest methodsFor: 'asserts' stamp: 'spb 10/13/2019 08:52:44'!
assertGanoO

	self assert: tateti ganoO .
	self deny: tateti ganoX .
	! !

!TerniLapilliTest methodsFor: 'asserts' stamp: 'spb 10/13/2019 08:52:36'!
assertGanoX

	self assert: tateti ganoX .
	self deny: tateti ganoO .
	! !

!TerniLapilliTest methodsFor: 'asserts' stamp: 'spb 10/13/2019 08:55:25'!
assertNadieGano

	self deny: tateti ganoX .
	self deny: tateti ganoO .
	! !

!TerniLapilliTest methodsFor: 'asserts' stamp: 'spb 10/12/2019 16:58:47'!
assertPosicionesDeO: unConjuntoDePosiciones
	
	self assert: tateti posicionesDeO size = unConjuntoDePosiciones size .
	unConjuntoDePosiciones do: [
		:unaPosicion |
		self assert: tateti posicionesDeO includes: unaPosicion .
	] .


	! !

!TerniLapilliTest methodsFor: 'asserts' stamp: 'spb 10/12/2019 16:45:48'!
assertPosicionesDeX: unConjuntoDePosiciones
	
	self assert: tateti posicionesDeX size = unConjuntoDePosiciones size .
	unConjuntoDePosiciones do: [
		:unaPosicion |
		self assert: tateti posicionesDeX includes: unaPosicion .
	] .


	! !

!TerniLapilliTest methodsFor: 'asserts' stamp: 'spb 10/12/2019 16:58:31'!
assertPosicionesDeX: unConjuntoDePosicionesDeX deO: unConjuntoDePosicionesDeO

	self assertPosicionesDeX: unConjuntoDePosicionesDeX .
	self assertPosicionesDeO: unConjuntoDePosicionesDeO .

	! !

!TerniLapilliTest methodsFor: 'asserts' stamp: 'spb 10/13/2019 08:52:12'!
assertTurnoDeNadie

	self deny: tateti esTurnoDeO .
	self deny: tateti esTurnoDeX .
	! !

!TerniLapilliTest methodsFor: 'asserts' stamp: 'spb 10/12/2019 22:08:43'!
assertTurnoDeO

	self assert: tateti esTurnoDeO .
	self deny: tateti esTurnoDeX .
	! !

!TerniLapilliTest methodsFor: 'asserts' stamp: 'spb 10/12/2019 22:08:43'!
assertTurnoDeX

	self assert: tateti esTurnoDeX .
	self deny: tateti esTurnoDeO .
	! !


!classDefinition: #TerniLapilliTestColocandoFichas category: #TerniLapilli!
TerniLapilliTest subclass: #TerniLapilliTestColocandoFichas
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
setUp
	
	tateti := TerniLapilli new.! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'spb 10/12/2019 09:14:24'!
test01InicioDeJuegoJuegaX
	
	self assertTurnoDeX .! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'spb 10/12/2019 22:11:51'!
test02JuegaXDespuesJuegaO

	tateti colocaXUnaFichaEn: 0@0 .
	
	self assertTurnoDeO .! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'spb 10/12/2019 22:11:51'!
test03JuegaXJuegaODespuesJuegaX

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	self assertTurnoDeX .! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test04NoPuedeJugarXDosVecesSeguidas

	tateti colocaXUnaFichaEn: 0@0 .
	
	self should: [
		tateti colocaXUnaFichaEn: 1@0 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeNoEsTuTurno equals: unError messageText .
		self assertTurnoDeO 
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test05NoPuedeJugarODosVecesSeguidas

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	self should: [
		tateti colocaOUnaFichaEn: 0@1 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeNoEsTuTurno equals: unError messageText .
		self assertTurnoDeX 
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test06NoPuedeJugarXFueraDelTablero

	self shouldFail: [ tateti colocaXUnaFichaEn: 2@0 . ] .

	self should: [
		tateti colocaXUnaFichaEn: -2@0 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoFueraDelTablero equals: unError messageText .
		self assertTurnoDeX .
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test07NoPuedeJugarXFueraDelTableroEnVariosIntentos

	self shouldFail: [ tateti colocaXUnaFichaEn: 2@0 . ] .

	self should: [
		tateti colocaXUnaFichaEn: -2@0 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoFueraDelTablero equals: unError messageText .
		self assertTurnoDeX .
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test08NoPuedeJugarOFueraDelTableroEnVariosIntentos

	tateti colocaXUnaFichaEn: 0@0 .

	self shouldFail: [ tateti colocaOUnaFichaEn: 2@0 . ] .

	self should: [
		tateti colocaOUnaFichaEn: -2@0 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoFueraDelTablero equals: unError messageText .
		self assertTurnoDeO .
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:04'!
test09NoPuedeJuegarXEnUnaPosicionOcupadaPorX

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	self should: [
		tateti colocaXUnaFichaEn: 0@0 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoEnUnaPosicionOcupada equals: unError messageText .
		self assertTurnoDeX .
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test10NoPuedeJugarOEnUnaPosicionOcupadaPorX

	tateti colocaXUnaFichaEn: 0@0 .
	
	self should: [
		tateti colocaOUnaFichaEn: 0@0 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoEnUnaPosicionOcupada equals: unError messageText .
		self assertTurnoDeO .
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:40:36'!
test11NoPuedeJugarXEnUnaPosicionOcupadaPorO

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	self should: [
		tateti colocaXUnaFichaEn: 1@0 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoEnUnaPosicionOcupada equals: unError messageText .
		self assertTurnoDeX .
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test12NoPuedeJugarOEnUnaPosicionOcupadaPorO

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	tateti colocaXUnaFichaEn: -1@0 .
	
	self should: [
		tateti colocaOUnaFichaEn: 1@0 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoEnUnaPosicionOcupada equals: unError messageText .
		self assertTurnoDeO .
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test13NoPuedeOColocarMasDeTresFichas

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@1 .
	
	tateti colocaXUnaFichaEn: 0@1 .
	tateti colocaOUnaFichaEn: 0@-1 .
	
	tateti colocaXUnaFichaEn: 1@-1 .
	tateti colocaOUnaFichaEn: -1@1 .
	
	tateti mueveXDesde: 1@-1 A: 1@0 .
	
	self should: [
		tateti colocaOUnaFichaEn: -1@-1 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeYaNoPodesColocarMasFichas equals: unError messageText .
		self assertTurnoDeO .
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test14NoPuedeXColocarMasDeTresFichas

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	tateti colocaXUnaFichaEn: 1@1 .
	tateti colocaOUnaFichaEn: 0@1 .
	
	tateti colocaXUnaFichaEn: -1@1 .
	tateti colocaOUnaFichaEn: -1@0 .
	
	self should: [
		tateti colocaXUnaFichaEn: -1@-1 .
	]
	raise: Error
	withExceptionDo: [ 
		:unError |
		self assert: TerniLapilli errorMensajeYaNoPodesColocarMasFichas equals: unError messageText .
		self assertTurnoDeX .
	].
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:01:17'!
test15SeMantieneLaPosicionDeX

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	self assertPosicionesDeX: { (0@0) } .
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:01:13'!
test16SeMantienenLasPosicionesDeX

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	tateti colocaXUnaFichaEn: 0@1 .
	tateti colocaOUnaFichaEn: -1@0 .
	
	self assertPosicionesDeX: {(0@0). (0@1)} .
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:01:08'!
test17SeMantienenLasPosicionesDeXyO

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	tateti colocaXUnaFichaEn: 0@1 .
	tateti colocaOUnaFichaEn: -1@0 .
	
	self assertPosicionesDeX: {(0@0). (0@1)} deO: {(1@0). (-1@0)} .
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test18NoPuedeMoverOSinColocarTodasLasFichas

	tateti colocaXUnaFichaEn: 0@0 .
	
	self should: [
		tateti mueveODesde: 0@0 A: -1@0 . 
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeNoPodesMoverHastaNoColocarTodasTusFichas = unError messageText .
		self assertPosicionesDeX: {(0@0)} deO: {} .
	] 
	
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test19NoPuedeMoverXSinColocarTodasLasFichas

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	self should: [
		tateti mueveXDesde: 0@0 A: -1@0 . 
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeNoPodesMoverHastaNoColocarTodasTusFichas = unError messageText .
		self assertPosicionesDeX: {(0@0)} deO: {(1@0)} .
	] 
	
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:02:25'!
test20NoGanoNadie

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .
	
	self assertNadieGano 
	
! !

!TerniLapilliTestColocandoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test21GanaX

	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@0 .

	tateti colocaXUnaFichaEn: 0@1 .
	tateti colocaOUnaFichaEn: 1@1 .

	tateti colocaXUnaFichaEn: 0@-1 .
	
	self assertGanoX.
	
	self should: [
		tateti colocaOUnaFichaEn: -1@1.
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajePartidaYaTerminada = unError messageText .
	] 
	
! !


!classDefinition: #TerniLapilliTestMoviendoFichas category: #TerniLapilli!
TerniLapilliTest subclass: #TerniLapilliTestMoviendoFichas
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
setUp
	
	tateti := TerniLapilli new.
	
	tateti colocaXUnaFichaEn: 0@0 .
	tateti colocaOUnaFichaEn: 1@1 .
	
	tateti colocaXUnaFichaEn: 0@1 .
	tateti colocaOUnaFichaEn: 0@-1 .
	
	tateti colocaXUnaFichaEn: 1@-1 .
	tateti colocaOUnaFichaEn: -1@1 .! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test01NoPuedeMoverOEnElTurnoDeX
	
	self should: [
		tateti mueveODesde: 1@-1 A: 1@0 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeNoEsTuTurno = unError messageText .
		self assertPosicionesDeX: {(0@0). (0@1). (1@-1)} deO: {(1@1). (0@-1). (-1@1)} .
		self assertTurnoDeX .
	] 
	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test02NoPuedeMoverXAUnaPosicionFueraDelTableo
	
	self should: [
		tateti mueveXDesde: 1@-1 A: 100@0 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoFueraDelTablero = unError messageText .
		self assertPosicionesDeX: {(0@0). (0@1). (1@-1)} deO: {(1@1). (0@-1). (-1@1)} .
		self assertTurnoDeX .
	] 
	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test03NoPuedeMoverXAUnaPosicionYaOcupadaPorO
	
	self should: [
		tateti mueveXDesde: 1@-1 A: 1@1 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoEnUnaPosicionOcupada = unError messageText .
		self assertPosicionesDeX: {(0@0). (0@1). (1@-1)} deO: {(1@1). (0@-1). (-1@1)} .
		self assertTurnoDeX .
	] 
	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test04NoPuedeMoverXAUnaPosicionYaOcupadaPorX
	
	self should: [
		tateti mueveXDesde: 1@-1 A: 0@0 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoEnUnaPosicionOcupada = unError messageText .
		self assertPosicionesDeX: {(0@0). (0@1). (1@-1)} deO: {(1@1). (0@-1). (-1@1)} .
		self assertTurnoDeX .
	] 
	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test05NoPuedeMoverXDesdeUnaPosicionQueNoEsteOcupadaPorX
	
	self should: [
		tateti mueveXDesde: 0@-1 A: -1@-1 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeNoTenesFichaEnEsaPosicion = unError messageText .
		self assertPosicionesDeX: {(0@0). (0@1). (1@-1)} deO: {(1@1). (0@-1). (-1@1)} .
		self assertTurnoDeX .
	] 
	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'spb 10/13/2019 08:43:34'!
test06MueveXSatisfactoriamente
	
	tateti mueveXDesde: 1@-1 A: 1@0 .

	self assertPosicionesDeX: {(0@0). (0@1). (1@0)} deO: {(1@1). (0@-1). (-1@1)} .
	self assertTurnoDeO .
	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'spb 10/13/2019 08:47:16'!
test07MueveXMueveOSatisfactoriamente
	
	tateti mueveXDesde: 1@-1 A: 1@0 .
	tateti mueveODesde: -1@1 A: -1@0 .
	
	self assertPosicionesDeX: {(0@0). (0@1). (1@0)} deO: {(1@1). (0@-1). (-1@0)} .
	self assertTurnoDeX .
	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test08MueveXYGana
	
	tateti mueveXDesde: 1@-1 A: 1@0 .
	tateti mueveODesde: -1@1 A: -1@0 .
	
	tateti mueveXDesde: 1@0 A: 1@-1 .
	tateti mueveODesde: -1@0 A: -1@-1 .
	
	tateti mueveXDesde: 0@1 A: -1@1 .
	
	self assertGanoX .
	self assertPosicionesDeX: {(0@0). (-1@1). (1@-1)} deO: {(1@1). (0@-1). (-1@-1)} .
	self assert: tateti terminoPartida.
	
	self should: [
		tateti colocaXUnaFichaEn: 1@-1.
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajePartidaYaTerminada = unError messageText .
	].

	self should: [
		tateti mueveODesde: 1@-1 A: -1@0 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajePartidaYaTerminada = unError messageText .
	].! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:15:54'!
test09MueveXYNoGana
	
	tateti mueveXDesde: 1@-1 A: 1@0 .
	tateti mueveODesde: -1@1 A: -1@0 .
	
	tateti mueveXDesde: 1@0 A: 1@-1 .
	
	self assertPosicionesDeX: {(0@0). (0@1). (1@-1)} deO: {(1@1). (0@-1). (-1@0)} .
	self assertNadieGano	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test10NoPuedeMoverAPosicionNoAlcanzable
	
	self should: [
		tateti mueveXDesde: 1@-1 A: -1@0 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeMovimientoInvalidoPorqueNoEsAlcanzable = unError messageText .
	].

	self should: [
		tateti mueveXDesde: 0@1 A: 1@0 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeMovimientoInvalidoPorqueNoEsAlcanzable = unError messageText .
	] 	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test11NoPuedeSaltearUnaFichaParaMoverseAUnaPosicionAlcanzable
	
	self should: [
		tateti mueveXDesde: 1@-1 A: -1@-1 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeMovimientoInvalidoPorPosicionesIntermedias = unError messageText .
	].

	tateti mueveXDesde: 1@-1 A: 1@0 .

	self should: [
		tateti mueveODesde: 1@1 A: -1@-1 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeMovimientoInvalidoPorPosicionesIntermedias = unError messageText .
	]. 	

	self should: [
		tateti mueveODesde: 1@1 A: 1@-1 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeMovimientoInvalidoPorPosicionesIntermedias = unError messageText .
	] 	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test12NoSePuedeMoverAUnaPosicionFueraDelTablero
	
	self should: [
		tateti mueveXDesde: 1@8 A: -1@-1 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoFueraDelTablero = unError messageText .
	].

	self should: [
		tateti mueveXDesde: -3@1 A: -1@-1 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoFueraDelTablero = unError messageText .
	]	! !

!TerniLapilliTestMoviendoFichas methodsFor: 'tests' stamp: 'AC 10/17/2019 02:34:03'!
test13NoSePuedeMoverDesdeUnaPosicionFueraDelTablero
	
	self should: [
		tateti mueveXDesde: 1@-1 A: 8@-1 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoFueraDelTablero = unError messageText .
	].

	tateti mueveXDesde: 1@-1 A: 1@0 .

	self should: [
		tateti mueveODesde: 1@1 A: -9@-1 .
	] 
	raise: Error
	withExceptionDo: [
		:unError |
		self assert: TerniLapilli errorMensajeEstasJugandoFueraDelTablero = unError messageText .
	]! !


!classDefinition: #Estado category: #TerniLapilli!
Object subclass: #Estado
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Estado class' category: #TerniLapilli!
Estado class
	instanceVariableNames: ''!

!Estado class methodsFor: 'assert etapa' stamp: 'AC 10/17/2019 01:28:52'!
assertColocamientoDeFichas

	self subclassResponsibility ! !

!Estado class methodsFor: 'assert etapa' stamp: 'AC 10/17/2019 01:28:54'!
assertMovimientoDeFichas

	self subclassResponsibility ! !


!Estado class methodsFor: 'observadores' stamp: 'AC 10/17/2019 01:28:58'!
terminoPartida

	self subclassResponsibility ! !


!classDefinition: #PartidaEnCurso category: #TerniLapilli!
Estado subclass: #PartidaEnCurso
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PartidaEnCurso class' category: #TerniLapilli!
PartidaEnCurso class
	instanceVariableNames: ''!

!PartidaEnCurso class methodsFor: 'assert etapa' stamp: 'AC 10/17/2019 01:29:46'!
assertColocamientoDeFichas! !

!PartidaEnCurso class methodsFor: 'assert etapa' stamp: 'AC 10/17/2019 02:34:03'!
assertMovimientoDeFichas

	self error: TerniLapilli errorMensajeNoPodesMoverHastaNoColocarTodasTusFichas ! !


!PartidaEnCurso class methodsFor: 'observadores' stamp: 'AC 10/17/2019 01:32:09'!
terminoPartida

	^false! !


!classDefinition: #EtapaColocacion category: #TerniLapilli!
PartidaEnCurso subclass: #EtapaColocacion
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EtapaColocacion class' category: #TerniLapilli!
EtapaColocacion class
	instanceVariableNames: ''!

!EtapaColocacion class methodsFor: 'assert etapa' stamp: 'AC 10/17/2019 00:49:19'!
assertColocamientoDeFichas! !

!EtapaColocacion class methodsFor: 'assert etapa' stamp: 'AC 10/17/2019 02:34:03'!
assertMovimientoDeFichas

	self error: TerniLapilli errorMensajeNoPodesMoverHastaNoColocarTodasTusFichas ! !



!classDefinition: #EtapaMovimiento category: #TerniLapilli!
PartidaEnCurso subclass: #EtapaMovimiento
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EtapaMovimiento class' category: #TerniLapilli!
EtapaMovimiento class
	instanceVariableNames: ''!

!EtapaMovimiento class methodsFor: 'assert etapa' stamp: 'AC 10/17/2019 02:34:03'!
assertColocamientoDeFichas

	self error: TerniLapilli errorMensajeYaNoPodesColocarMasFichas ! !

!EtapaMovimiento class methodsFor: 'assert etapa' stamp: 'AC 10/17/2019 00:49:33'!
assertMovimientoDeFichas! !



!classDefinition: #PartidaTerminada category: #TerniLapilli!
Estado subclass: #PartidaTerminada
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PartidaTerminada class' category: #TerniLapilli!
PartidaTerminada class
	instanceVariableNames: ''!

!PartidaTerminada class methodsFor: 'assert etapa' stamp: 'AC 10/17/2019 02:34:03'!
assertColocamientoDeFichas

	self error: TerniLapilli errorMensajePartidaYaTerminada ! !

!PartidaTerminada class methodsFor: 'assert etapa' stamp: 'AC 10/17/2019 02:34:03'!
assertMovimientoDeFichas

	self error: TerniLapilli errorMensajePartidaYaTerminada ! !


!PartidaTerminada class methodsFor: 'observadores' stamp: 'AC 10/17/2019 01:32:15'!
terminoPartida

	^true! !


!classDefinition: #TerniLapilli category: #TerniLapilli!
Object subclass: #TerniLapilli
	instanceVariableNames: 'turno estado ganador jugadorX jugadorO'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilli methodsFor: 'modificadores' stamp: 'AC 10/17/2019 02:24:55'!
colocaJugador: unJugador UnaFichaEn: unaPosicion 

	self validarPosicionVacia: unaPosicion .
	
	unJugador agregarFichaEn: unaPosicion .
		
	self pasarTurno .
	
	! !

!TerniLapilli methodsFor: 'modificadores' stamp: 'AC 10/17/2019 02:27:35'!
mueveJugador: unJugador Desde: unaPosicionOrigen A: unaPosicionDestino

	self validarPosicionOcupada: unaPosicionOrigen por: unJugador.
	
	self validarPosicionVacia: unaPosicionDestino .
	
	self verificarMovimientoValidoDesde: unaPosicionOrigen hasta: unaPosicionDestino.
	
	unJugador moverFichaDesde: unaPosicionOrigen A: unaPosicionDestino .
	
	self pasarTurno .! !


!TerniLapilli methodsFor: 'observadores de tablero' stamp: 'AC 10/17/2019 02:08:25'!
checkearVictoriaDe: unJugador

	unJugador gano ifTrue: [estado := PartidaTerminada. ganador := unJugador]! !

!TerniLapilli methodsFor: 'observadores de tablero' stamp: 'AC 10/17/2019 02:03:53'!
ganoO

	^ganador == jugadorO! !

!TerniLapilli methodsFor: 'observadores de tablero' stamp: 'AC 10/17/2019 02:03:56'!
ganoX

	^ganador == jugadorX! !

!TerniLapilli methodsFor: 'observadores de tablero' stamp: 'spb 10/12/2019 22:04:48'!
posicionesDe: unJugador

	^unJugador fichas .! !

!TerniLapilli methodsFor: 'observadores de tablero' stamp: 'spb 10/12/2019 22:04:44'!
posicionesDeO

	^self posicionesDe: jugadorO .! !

!TerniLapilli methodsFor: 'observadores de tablero' stamp: 'spb 10/12/2019 22:04:40'!
posicionesDeX

	^self posicionesDe: jugadorX .! !

!TerniLapilli methodsFor: 'observadores de tablero' stamp: 'AC 10/17/2019 02:28:55'!
validarPosicionOcupada: unaPosicion por: unJugador

	(self posicionValida: unaPosicion) ifFalse: [self error: self class errorMensajeEstasJugandoFueraDelTablero . ] .
	((self posicionesDe: unJugador) includes: unaPosicion) ifFalse: [ self error: self class errorMensajeNoTenesFichaEnEsaPosicion . ] .
	
	! !

!TerniLapilli methodsFor: 'observadores de tablero' stamp: 'AC 10/17/2019 02:24:55'!
validarPosicionVacia: unaPosicion 

	(self posicionValida: unaPosicion) ifFalse: [self error: self class errorMensajeEstasJugandoFueraDelTablero . ] .
	(self posicionVacia: unaPosicion) ifFalse: [self error: self class errorMensajeEstasJugandoEnUnaPosicionOcupada . ] .
	
	! !

!TerniLapilli methodsFor: 'observadores de tablero' stamp: 'AC 10/17/2019 00:04:16'!
verificarMovimientoValidoDesde: unaPosicionOrigen hasta: unaPosicionDestino

	(self posicionEsAlcanzable: unaPosicionDestino desde: unaPosicionOrigen) ifFalse: [
		self error: self class errorMensajeMovimientoInvalidoPorqueNoEsAlcanzable 
		].
	
	(self pasoIntermedioDisponibleSiNecesarioEntre: unaPosicionOrigen y: unaPosicionDestino) ifFalse: [
		self error: self class errorMensajeMovimientoInvalidoPorPosicionesIntermedias 
		].
	
	! !


!TerniLapilli methodsFor: 'turno' stamp: 'AC 10/17/2019 02:38:56'!
esTurnoDeO

	^turno esTurnoDeO ! !

!TerniLapilli methodsFor: 'turno' stamp: 'AC 10/17/2019 02:39:10'!
esTurnoDeX

	^turno esTurnoDeX ! !

!TerniLapilli methodsFor: 'turno' stamp: 'AC 10/15/2019 22:04:04'!
pasarTurno

	turno := turno proximoTurno .! !

!TerniLapilli methodsFor: 'turno' stamp: 'AC 10/17/2019 00:49:01'!
terminoPartida

	^estado terminoPartida! !


!TerniLapilli methodsFor: 'inicializacion' stamp: 'AC 10/17/2019 02:39:53'!
initialize

	turno := TurnoX .
	estado:= EtapaColocacion.
	ganador := nil.
	
	jugadorX := TerniLapilliJugador new.
	jugadorO := TerniLapilliJugador new.! !


!TerniLapilli methodsFor: 'acciones publicas' stamp: 'AC 10/17/2019 02:14:43'!
colocaOUnaFichaEn: unaPosicion 
	
	estado assertColocamientoDeFichas.

	turno assertTurnoDeO.

	self colocaJugador: jugadorO UnaFichaEn: unaPosicion . 

	jugadorO cantidadDeFichas = 3 ifTrue: [estado := EtapaMovimiento].
	
	self checkearVictoriaDe: jugadorO! !

!TerniLapilli methodsFor: 'acciones publicas' stamp: 'AC 10/17/2019 02:14:46'!
colocaXUnaFichaEn: unaPosicion 
	
	estado assertColocamientoDeFichas.

	turno assertTurnoDeX.

	self colocaJugador: jugadorX UnaFichaEn: unaPosicion . 
	
	self checkearVictoriaDe: jugadorX! !

!TerniLapilli methodsFor: 'acciones publicas' stamp: 'AC 10/17/2019 02:15:26'!
mueveODesde: unaPosicionOrigen A: unaPosicionDestino
	
	estado assertMovimientoDeFichas.

	turno assertTurnoDeO.

	self mueveJugador: jugadorO Desde: unaPosicionOrigen A: unaPosicionDestino .
	
	self checkearVictoriaDe: jugadorO
	! !

!TerniLapilli methodsFor: 'acciones publicas' stamp: 'AC 10/17/2019 02:15:28'!
mueveXDesde: unaPosicionOrigen A: unaPosicionDestino
	
	estado assertMovimientoDeFichas.

	turno assertTurnoDeX.

	self mueveJugador: jugadorX Desde: unaPosicionOrigen A: unaPosicionDestino .
	
	self checkearVictoriaDe: jugadorX
! !


!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/16/2019 23:24:41'!
estanEnMismaColumna: posicion1 y: posicion2

	^ posicion1 y = posicion2 y! !

!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/16/2019 23:37:30'!
estanEnMismaDiagonal: posicion1 y: posicion2

	^ (self estanEnMismaDiagonalPositiva: posicion1 y: posicion2) or: (self estanEnMismaDiagonalNegativa: posicion1 y: posicion2)! !

!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/16/2019 23:35:52'!
estanEnMismaDiagonalNegativa: posicion1 y: posicion2

	^ (posicion1 x = (0 - posicion1 y)) and: (posicion2 x = (0 - posicion2 y)).! !

!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/16/2019 23:35:42'!
estanEnMismaDiagonalPositiva: posicion1 y: posicion2

	^ (posicion1 x = posicion1 y) and: (posicion2 x = posicion2 y)! !

!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/16/2019 23:24:22'!
estanEnMismaFila: posicion1 y: posicion2

	^ posicion1 x = posicion2 x! !

!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/16/2019 23:54:39'!
movimientoRequierePasoIntermedioEntre: unaPosicionOrigen y: unaPosicionDestino

	^ ((unaPosicionOrigen x - unaPosicionDestino x) = 2) or: ((unaPosicionOrigen y - unaPosicionDestino y) = 2) ! !

!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/17/2019 00:20:57'!
pasoIntermedioDisponibleSiNecesarioEntre: unaPosicionOrigen y: unaPosicionDestino

	(self movimientoRequierePasoIntermedioEntre: unaPosicionOrigen y: unaPosicionDestino) ifFalse: [^true].
	
	^self posicionVacia: (self pasoIntermedioRequeridoEntre: unaPosicionOrigen y: unaPosicionDestino)! !

!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/16/2019 23:57:17'!
pasoIntermedioRequeridoEntre: unaPosicionOrigen y: unaPosicionDestino

	(self estanEnMismaColumna: unaPosicionOrigen y: unaPosicionDestino) ifTrue: [
		^0@(unaPosicionOrigen y)
		].
	
	(self estanEnMismaFila: unaPosicionOrigen y: unaPosicionDestino) ifTrue: [
		^(unaPosicionOrigen x)@0
		].
	
	^0@0! !

!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/16/2019 23:59:21'!
posicionEsAlcanzable: unaPosicionDestino desde: unaPosicionOrigen

	^(self estanEnMismaColumna: unaPosicionOrigen y: unaPosicionDestino) or: (self estanEnMismaFila: unaPosicionOrigen y: unaPosicionDestino) or:
	(self estanEnMismaDiagonal: unaPosicionOrigen y: unaPosicionDestino)! !

!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/17/2019 02:36:55'!
posicionVacia: unaPosicion

	^((self posicionesDeX includes: unaPosicion) or: (self posicionesDeO includes: unaPosicion)) not
	! !

!TerniLapilli methodsFor: 'posiciones' stamp: 'AC 10/17/2019 02:38:27'!
posicionValida: unaPosicion

	^(((unaPosicion x < -1) or: (1 < unaPosicion x)) or: ((unaPosicion y < -1) or: (1 < unaPosicion y))) not
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TerniLapilli class' category: #TerniLapilli!
TerniLapilli class
	instanceVariableNames: ''!

!TerniLapilli class methodsFor: 'errores' stamp: 'spb 10/12/2019 20:55:32'!
errorMensajeEstasJugandoEnUnaPosicionOcupada
	^'Estas jugando en una posicion ocupada'! !

!TerniLapilli class methodsFor: 'errores' stamp: 'spb 10/12/2019 20:55:40'!
errorMensajeEstasJugandoFueraDelTablero
	^'Estas jugando fuera del tablero'! !

!TerniLapilli class methodsFor: 'errores' stamp: 'AC 10/15/2019 22:59:54'!
errorMensajeMovimientoInvalidoPorPosicionesIntermedias

	^'No se puede llegar a esa nueva posici�n por presencia de otras fichas en el camino'! !

!TerniLapilli class methodsFor: 'errores' stamp: 'AC 10/15/2019 23:00:17'!
errorMensajeMovimientoInvalidoPorqueNoEsAlcanzable

	^'Esa nueva posicion no es alcanzable'! !

!TerniLapilli class methodsFor: 'errores' stamp: 'AC 10/17/2019 01:40:18'!
errorMensajeNadieGano

	^'Nadie gano todavia'! !

!TerniLapilli class methodsFor: 'errores' stamp: 'spb 10/12/2019 20:56:06'!
errorMensajeNoEsTuTurno
	^'No es tu turno'! !

!TerniLapilli class methodsFor: 'errores' stamp: 'spb 10/12/2019 20:56:25'!
errorMensajeNoPodesMoverHastaNoColocarTodasTusFichas
	^'No podes mover hasta no colocar todas tus fichas'! !

!TerniLapilli class methodsFor: 'errores' stamp: 'spb 10/13/2019 08:23:52'!
errorMensajeNoTenesFichaEnEsaPosicion
	^'No tenes fichas en esa posicion'! !

!TerniLapilli class methodsFor: 'errores' stamp: 'AC 10/17/2019 01:33:10'!
errorMensajePartidaYaTerminada

	^'La partida ya termino'! !

!TerniLapilli class methodsFor: 'errores' stamp: 'spb 10/12/2019 20:56:33'!
errorMensajeYaNoPodesColocarMasFichas
	^'Ya no podes colocar mas fichas'! !


!classDefinition: #TerniLapilliJugador category: #TerniLapilli!
Object subclass: #TerniLapilliJugador
	instanceVariableNames: 'fichas'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilliJugador methodsFor: 'observadores' stamp: 'spb 10/12/2019 21:54:27'!
cantidadDeFichas
	^fichas size! !

!TerniLapilliJugador methodsFor: 'observadores' stamp: 'spb 10/12/2019 21:35:29'!
fichas
	^fichas ! !

!TerniLapilliJugador methodsFor: 'observadores' stamp: 'AC 10/17/2019 01:55:36'!
gano

	(self satisfizoLaLinea: self class horizontalSuperior) ifTrue: [ ^ true . ] .
	(self satisfizoLaLinea: self class horizontalCentral ) ifTrue: [ ^ true . ] .
	(self satisfizoLaLinea: self class horizontalInferior ) ifTrue: [ ^ true . ] .
	(self satisfizoLaLinea: self class verticalIzquierda ) ifTrue: [ ^ true . ] .
	(self satisfizoLaLinea: self class verticalCentral ) ifTrue: [ ^ true . ] .
	(self satisfizoLaLinea: self class verticalDerecha ) ifTrue: [ ^ true . ] .
	(self satisfizoLaLinea: self class diagonalAscendente ) ifTrue: [ ^ true . ] .
	(self satisfizoLaLinea: self class diagonalDescendente ) ifTrue: [ ^ true . ] .

	^ false ! !

!TerniLapilliJugador methodsFor: 'observadores' stamp: 'AC 10/17/2019 01:57:02'!
satisfizoLaLinea: unaLinea
	^ unaLinea allSatisfy: [:value | fichas includes: value.] .! !


!TerniLapilliJugador methodsFor: 'modificadores' stamp: 'spb 10/12/2019 21:48:35'!
agregarFichaEn: unaPosicion

	fichas add: unaPosicion .! !

!TerniLapilliJugador methodsFor: 'modificadores' stamp: 'spb 10/13/2019 08:45:12'!
moverFichaDesde: unaPosicionOrigen A: unaPosicionDestino .

	fichas remove: unaPosicionOrigen .
	fichas add: unaPosicionDestino .! !


!TerniLapilliJugador methodsFor: 'iniciacion' stamp: 'spb 10/12/2019 21:26:48'!
initialize
	
	fichas := Set new .! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TerniLapilliJugador class' category: #TerniLapilli!
TerniLapilliJugador class
	instanceVariableNames: ''!

!TerniLapilliJugador class methodsFor: 'lineas ganadoras' stamp: 'spb 10/13/2019 09:17:31'!
diagonalAscendente
	^{(-1@-1). (0@0). (1@1)}! !

!TerniLapilliJugador class methodsFor: 'lineas ganadoras' stamp: 'spb 10/13/2019 09:17:43'!
diagonalDescendente
	^{(-1@1). (0@0). (1@-1)}! !

!TerniLapilliJugador class methodsFor: 'lineas ganadoras' stamp: 'AC 10/17/2019 01:54:06'!
horizontalCentral
	^{(0@-1). (0@0). (0@1)}! !

!TerniLapilliJugador class methodsFor: 'lineas ganadoras' stamp: 'AC 10/17/2019 01:54:20'!
horizontalInferior
	^{(-1@-1). (-1@0). (-1@1)}! !

!TerniLapilliJugador class methodsFor: 'lineas ganadoras' stamp: 'AC 10/17/2019 01:54:30'!
horizontalSuperior
	^{(1@-1). (1@0). (1@1)}! !

!TerniLapilliJugador class methodsFor: 'lineas ganadoras' stamp: 'AC 10/17/2019 01:54:50'!
verticalCentral
	^{(-1@1). (0@0). (1@0)}! !

!TerniLapilliJugador class methodsFor: 'lineas ganadoras' stamp: 'AC 10/17/2019 01:55:03'!
verticalDerecha
	^{(-1@1). (0@1). (1@1)}! !

!TerniLapilliJugador class methodsFor: 'lineas ganadoras' stamp: 'AC 10/17/2019 01:55:36'!
verticalIzquierda
	^{(-1@-1). (0@-1). (1@-1)}! !


!classDefinition: #Turno category: #TerniLapilli!
Object subclass: #Turno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Turno class' category: #TerniLapilli!
Turno class
	instanceVariableNames: ''!

!Turno class methodsFor: 'mensajes' stamp: 'AC 10/17/2019 02:20:40'!
assertTurnoDeO
	
	self subclassResponsibility! !

!Turno class methodsFor: 'mensajes' stamp: 'AC 10/17/2019 02:20:38'!
assertTurnoDeX
	
	self subclassResponsibility! !

!Turno class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:05:59'!
esTurnoDeO

	self subclassResponsibility ! !

!Turno class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:05:58'!
esTurnoDeX

	self subclassResponsibility ! !

!Turno class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:04:46'!
proximoTurno

	self subclassResponsibility ! !


!classDefinition: #TurnoO category: #TerniLapilli!
Turno subclass: #TurnoO
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TurnoO class' category: #TerniLapilli!
TurnoO class
	instanceVariableNames: ''!

!TurnoO class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:09:46'!
assertTurnoDeO! !

!TurnoO class methodsFor: 'mensajes' stamp: 'AC 10/17/2019 02:34:03'!
assertTurnoDeX

	self error: TerniLapilli errorMensajeNoEsTuTurno ! !

!TurnoO class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:06:20'!
esTurnoDeO

	^true! !

!TurnoO class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:06:16'!
esTurnoDeX

	^false! !

!TurnoO class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:05:10'!
proximoTurno

	^TurnoX! !


!classDefinition: #TurnoX category: #TerniLapilli!
Turno subclass: #TurnoX
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TurnoX class' category: #TerniLapilli!
TurnoX class
	instanceVariableNames: ''!

!TurnoX class methodsFor: 'mensajes' stamp: 'AC 10/17/2019 02:34:03'!
assertTurnoDeO

	self error: TerniLapilli errorMensajeNoEsTuTurno ! !

!TurnoX class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:10:43'!
assertTurnoDeX! !

!TurnoX class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:06:28'!
esTurnoDeO

	^false! !

!TurnoX class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:06:32'!
esTurnoDeX

	^true! !

!TurnoX class methodsFor: 'mensajes' stamp: 'AC 10/15/2019 22:05:23'!
proximoTurno

	^TurnoO! !
