!classDefinition: #CantSuspend category: #'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: #'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: #'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'AC 9/2/2019 19:05:05'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook |
	
	customerBook := CustomerBook new.
	
	
	self testearTiempoDeEjecucion: [customerBook addCustomerNamed: 'John Lennon'.] paraMilisegundos: 50.
	
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'AC 9/2/2019 20:38:09'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney |
	
	paulMcCartney := 'Paul McCartney'.
	customerBook := CustomerBook prepararBookConCliente: paulMcCartney .
	
	
	
	self testearTiempoDeEjecucion: [customerBook removeCustomerNamed: paulMcCartney.] paraMilisegundos: 100.
	
	  ! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ssda 9/9/2019 02:21:31'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.

	self testearMensajeDeError: [ customerBook addCustomerNamed: ''.] 
		conTipoDeError: Error 
		enCasoDeError: [ :anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ].

	! !

!CustomerBookTest methodsFor: 'testing' stamp: 'ssda 9/9/2019 02:21:50'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	johnLennon := 'John Lennon'.
	customerBook := CustomerBook prepararBookConCliente: johnLennon .
	
	self testearMensajeDeError: [ customerBook removeCustomerNamed: 'Paul McCartney'.]
		conTipoDeError: NotFound 
		enCasoDeError: [ :anError | 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: johnLennon) ].
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'AC 9/2/2019 20:38:56'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	paulMcCartney := 'Paul McCartney'.
	customerBook := CustomerBook prepararBookConCliente: paulMcCartney .
	
	customerBook suspendCustomerNamed: paulMcCartney.
	
	self corroborarEstadoDelBook: customerBook numeroActivos: 0 numeroSuspendidos: 1 numeroClientes: 1.
	
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'AC 9/2/2019 20:39:13'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	paulMcCartney := 'Paul McCartney'.
	customerBook := CustomerBook prepararBookConCliente: paulMcCartney .
	
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self corroborarEstadoDelBook: customerBook numeroActivos: 0 numeroSuspendidos: 0 numeroClientes: 0.
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'AC 9/2/2019 20:39:23'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
			
	johnLennon := 'John Lennon'.
	customerBook := CustomerBook prepararBookConCliente: johnLennon .
	
	self testearCannotSuspend: 'Paul McCartney' onBook: customerBook  hasCustomer: johnLennon. 
	! !

!CustomerBookTest methodsFor: 'testing' stamp: 'AC 9/2/2019 20:39:31'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	johnLennon := 'John Lennon'.
	customerBook := CustomerBook prepararBookConCliente: johnLennon .
	
	customerBook suspendCustomerNamed: johnLennon.
	
	self testearCannotSuspend: johnLennon onBook: customerBook  hasCustomer: johnLennon. 
	
! !


!CustomerBookTest methodsFor: 'tiempo' stamp: 'AC 9/2/2019 19:04:44'!
medirTiempoDeEjecucion: aClosure

	|millisecondsBeforeRunning millisecondsAfterRunning| 

	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	aClosure value.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	^millisecondsAfterRunning-millisecondsBeforeRunning.! !

!CustomerBookTest methodsFor: 'tiempo' stamp: 'AC 9/2/2019 19:04:51'!
testearTiempoDeEjecucion: aClosure paraMilisegundos: milisegundos
	|tiempo|
	
	tiempo := self medirTiempoDeEjecucion: aClosure.
	
	self assert: tiempo < (milisegundos * millisecond).! !


!CustomerBookTest methodsFor: 'error' stamp: 'ssda 9/9/2019 02:21:17'!
testearMensajeDeError: aClosure conTipoDeError: tipoDeError enCasoDeError: errorClosure

	[aClosure value. self fail ]
		on: tipoDeError 
		do: errorClosure.
	! !


!CustomerBookTest methodsFor: 'book' stamp: 'AC 9/2/2019 19:29:00'!
corroborarEstadoDelBook: book numeroActivos: activos numeroSuspendidos: suspendidos numeroClientes: clientes

	self assert: activos equals: book numberOfActiveCustomers.
	self assert: suspendidos equals: book numberOfSuspendedCustomers.
	self assert: clientes equals: book numberOfCustomers.! !

!CustomerBookTest methodsFor: 'book' stamp: 'AC 9/2/2019 20:34:30'!
prepararBookConCliente: cliente

	|customerBook|

	customerBook := CustomerBook new.
	
	customerBook addCustomerNamed: cliente.
	
	^customerBook! !

!CustomerBookTest methodsFor: 'book' stamp: 'ssda 9/9/2019 02:22:08'!
testearCannotSuspend: customer onBook: customerBook hasCustomer: customerOnBook

	self testearMensajeDeError: [ customerBook suspendCustomerNamed: customer.] 
		conTipoDeError: CantSuspend 
		enCasoDeError: [ :anError | 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: customerOnBook) ]

	! !


!classDefinition: #CustomerBook category: #'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 4/3/2019 10:14:26'!
initialize

	super initialize.
	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'ssda 9/9/2019 02:30:10'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'ssda 9/9/2019 02:45:29'!
checkAndRemoveCustomer: aName fromList: list
	
	1 to: list size do: 
	[ :index |
		aName = (list at: index)
			ifTrue: [
				list removeAt: index.
				^ true 
			] 
	].

	^false! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'ssda 9/9/2019 02:24:21'!
numberOfCustomers
	
	^self numberOfActiveCustomers + self numberOfSuspendedCustomers ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'ssda 9/9/2019 02:47:39'!
removeCustomerNamed: aName 
 
	((self checkAndRemoveCustomer: aName fromList: active) or:
	[self checkAndRemoveCustomer: aName fromList: suspended.])
		ifTrue: [^aName.]
		ifFalse: [^ NotFound signal.]
	
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
suspendCustomerNamed: aName 
	
	(active includes: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: #'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 9/2/2019 14:41:10'!
customerAlreadyExistsErrorMessage

	^'Customer Already Exists.'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 9/2/2019 14:41:16'!
customerCanNotBeEmptyErrorMessage

	^'Customer Name Cannot Be Empty.'! !


!CustomerBook class methodsFor: 'creators' stamp: 'AC 9/2/2019 20:36:33'!
prepararBookConCliente: cliente

	|customerBook|

	customerBook := CustomerBook new.
	
	customerBook addCustomerNamed: cliente.
	
	^customerBook! !
