!classDefinition: #CartTest category: #'TusLibros-Ejercicio'!
TestCase subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Ejercicio'!

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 00:39:11'!
test01NewCartIsEmpty

	|cart|
	
	cart := Cart new.
	
	self assert: cart isEmpty.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 00:40:41'!
test02CartIsNotEmptyAfterAddingBook

	|cart|
	
	cart := Cart new.
	
	cart add: #book.
	
	self deny: cart isEmpty.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 00:42:56'!
test03NewCartHasZeroSize

	|cart|
	
	cart := Cart new.
	
	self assert: cart size = 0! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 00:43:26'!
test04CartSizeIsOneAfterAddingBook

	|cart|
	
	cart := Cart new.
	
	cart add: #book.
	
	self assert: cart size = 1! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 00:48:08'!
test05CartHasBookThatWasAdded

	|cart|
	
	cart := Cart new.
	
	self deny: (cart hasItem: #book).
	
	cart add: #book.
	
	self assert: (cart hasItem: #book).! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 00:49:23'!
test06CartHasBooksAddedAndNotOthers

	|cart|
	
	cart := Cart new.
	
	self deny: (cart hasItem: #book1).
	self deny: (cart hasItem: #book2).
	
	cart add: #book1.
	
	self assert: (cart hasItem: #book1).
	self deny: (cart hasItem: #book2).
	
	cart add: #book2.
	
	self assert: (cart hasItem: #book1).
	self assert: (cart hasItem: #book2).! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 00:54:43'!
test07CanAddMultipleBooksCorrectly

	|cart|
	
	cart := Cart new.
	
	cart add: #book times: 5.
	
	self assert: (cart hasItem: #book).
	self assert: cart size = 5! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 01:00:22'!
test08CantAddFewerThanOneBook

	|cart|
	
	cart := Cart new.
	
	self should: [cart add: #aBook times: 0] raise: Error
	 withExceptionDo: [:anError | self assert: anError messageText equals: Cart errorCantAddFewerThanOneBook. 
		self assert: cart isEmpty].! !


!classDefinition: #Cart category: #'TusLibros-Ejercicio'!
Object subclass: #Cart
	instanceVariableNames: 'content'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros-Ejercicio'!

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 00:50:14'!
hasItem: aBook

	^content includes: aBook! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 00:49:36'!
isEmpty

	^content isEmpty! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 00:49:44'!
size

	^content size! !


!Cart methodsFor: 'modifiers' stamp: 'AC 11/7/2019 00:49:53'!
add: aBook

	content add: aBook! !

!Cart methodsFor: 'modifiers' stamp: 'AC 11/7/2019 00:59:52'!
add: aBook times: quantity

	|bookToAdd|
	
	bookToAdd := 0.
	
	quantity < 1 ifTrue: [self error: self class errorCantAddFewerThanOneBook ].

	[bookToAdd < quantity] whileTrue: [self add: aBook. bookToAdd := bookToAdd +1]! !


!Cart methodsFor: 'initialization' stamp: 'AC 11/7/2019 00:50:05'!
initialize

	content := OrderedCollection new! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: #'TusLibros-Ejercicio'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'errors' stamp: 'AC 11/7/2019 00:59:03'!
errorCantAddFewerThanOneBook

	^'Cant add fewer than one book to the cart'! !
