!classDefinition: #CartTest category: #TusLibros!
TestCase subclass: #CartTest
	instanceVariableNames: 'cart testObjectFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:06:07'!
setUp
	
	testObjectFactory := StoreTestObjectsFactory new.
	
	cart := testObjectFactory createCart .! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:46:47'!
test01NewCartIsEmpty
	
	self assert: cart isEmpty.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:00:56'!
test02CartIsNotEmptyAfterAddingBook
	
	cart add: testObjectFactory itemSoldByTheStore1.
	
	self deny: cart isEmpty.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:34:41'!
test03NewCartHasZeroSize
	
	self assert: cart size = 0! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:01:12'!
test04CartSizeIsOneAfterAddingBook
	
	cart add: testObjectFactory itemSoldByTheStore1.
	
	self assert: cart size = 1! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:01:26'!
test05CartHasBookThatWasAdded
	
	self deny: (cart hasItem: testObjectFactory itemSoldByTheStore1).
	
	cart add: testObjectFactory itemSoldByTheStore1.
	
	self assert: (cart hasItem: testObjectFactory itemSoldByTheStore1).! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:01:51'!
test06CartHasBooksAddedAndNotOthers
	
	self deny: (cart hasItem: testObjectFactory itemSoldByTheStore1).
	self deny: (cart hasItem: testObjectFactory itemSoldByTheStore2).
	
	cart add: testObjectFactory itemSoldByTheStore1.
	
	self assert: (cart hasItem: testObjectFactory itemSoldByTheStore1).
	self deny: (cart hasItem: testObjectFactory itemSoldByTheStore2).
	
	cart add: testObjectFactory itemSoldByTheStore2.
	
	self assert: (cart hasItem: testObjectFactory itemSoldByTheStore1).
	self assert: (cart hasItem: testObjectFactory itemSoldByTheStore2).! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:02:58'!
test07CanAddMultipleBooksCorrectly
	
	cart add: testObjectFactory itemSoldByTheStore1 times: 5.
	
	self assert: (cart hasItem: testObjectFactory itemSoldByTheStore1).
	self assert: cart size = 5! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:03:54'!
test08CantAddFewerThanOneBook
	
	self should: [cart add: testObjectFactory itemSoldByTheStore1 times: 0] raise: Error
	 withExceptionDo: [:anError | self assert: anError messageText equals: Cart errorCantAddFewerThanOneBook. 
		self assert: cart isEmpty].! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:04:16'!
test09CanCheckOccurrences
	
	self assert: (cart occurrencesOf: testObjectFactory itemSoldByTheStore1) = 0.
	
	cart add: testObjectFactory itemSoldByTheStore1 times: 8.
	
	self assert: (cart occurrencesOf: testObjectFactory itemSoldByTheStore1) = 8.
	
	cart add: testObjectFactory itemSoldByTheStore1 times: 7.
	
	self assert: (cart occurrencesOf: testObjectFactory itemSoldByTheStore1) = 15.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:05:17'!
test10CantAddBookThatIsntInCatalog


	self should: [cart add: testObjectFactory itemNotSoldByTheStore ] raise: Error
	withExceptionDo: [:anError | self assert: anError messageText equals: Cart errorCantAddBookOutOfCatalog.]! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:05:33'!
test11CanListCorrectly

	|list|

	cart add: testObjectFactory itemSoldByTheStore1 times: 5.
	cart add: testObjectFactory itemSoldByTheStore2 times: 7.
	
	list := cart list.
	
	self assert: (list at: testObjectFactory itemSoldByTheStore1) = 5.
	self assert: (list at: testObjectFactory itemSoldByTheStore2) = 7.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/16/2019 20:09:56'!
test12CanObserveTotalCostOfContent
	cart add: testObjectFactory itemSoldByTheStore1 times: 5.
	cart add: testObjectFactory itemSoldByTheStore2 times: 7.
			.
	
	self assert: cart costOfContent = ((testObjectFactory itemSoldByTheStore1Price * 5) + (testObjectFactory itemSoldByTheStore2Price * 7)).! !


!classDefinition: #CashierTest category: #TusLibros!
TestCase subclass: #CashierTest
	instanceVariableNames: 'cart creditCard salesHistory merchantProcessor testObjectFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'AC 11/16/2019 19:54:17'!
setUp
	testObjectFactory := StoreTestObjectsFactory new.
	
	cart := testObjectFactory createCart.
	creditCard := testObjectFactory notExpiredCreditCard .
	
	salesHistory := OrderedCollection new.
	
	merchantProcessor := MerchantProcessor new.
	
	! !

!CashierTest methodsFor: 'tests' stamp: 'AC 11/7/2019 13:08:00'!
test01CantCheckoutEmptyCart

	| cashier | 
	
	cashier := self createCashier.
	
	
	self should: [cashier checkout] raise: Error
	withExceptionDo: [:anError | self assert: Cashier errorCantCheckoutAnEmptyCart equals: anError messageText.]! !

!CashierTest methodsFor: 'tests' stamp: 'AC 11/16/2019 20:08:47'!
test02CheckoutRegistersSale

	| cashier sale | 
	
	cashier := self createCashier.
	
	cart add: testObjectFactory itemSoldByTheStore1.
	
	self assert: salesHistory isEmpty.
	
	cashier checkout.
	
	sale := salesHistory at: 1.
	
	self assertSale: sale isTo: 'Mengana' for: testObjectFactory itemSoldByTheStore1Price withItems: cart list.! !

!CashierTest methodsFor: 'tests' stamp: 'AC 11/16/2019 18:20:07'!
test03CantCheckoutWithExpiredCreditCard

	| cashier  | 
	
	creditCard := testObjectFactory expiredCreditCard .
	
	cashier := self createCashier.
	
	cart add: testObjectFactory itemSoldByTheStore1.
	
	self should: [cashier checkout] raise: Error
	withExceptionDo: [:anError | self assert: Cashier errorCantUseExpiredCard equals: anError messageText.]! !


!CashierTest methodsFor: 'support' stamp: 'AC 11/7/2019 13:35:37'!
assertSale: aSale isTo: aClient for: anAmmount withItems: aListOfItems

	self assert: aSale client = aClient.
	self assert: aSale ammount = anAmmount .
	self assert: aSale listOfItems = aListOfItems .! !

!CashierTest methodsFor: 'support' stamp: 'AC 11/16/2019 19:55:23'!
createCashier

	^Cashier forChecking: cart onDate: testObjectFactory today withCreditCard: creditCard withSalesHistory: salesHistory withMP: merchantProcessor! !


!classDefinition: #CreditCardTest category: #TusLibros!
TestCase subclass: #CreditCardTest
	instanceVariableNames: 'testObjectFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCardTest methodsFor: 'tests' stamp: 'AC 11/16/2019 17:47:03'!
setUp

	testObjectFactory := StoreTestObjectsFactory new! !

!CreditCardTest methodsFor: 'tests' stamp: 'AC 11/16/2019 17:50:47'!
test01FieldsCanBeObserved

	|creditCard |
	
	creditCard := CreditCard forClient: 'Mengana' withNumber: 1234567890123456 expiryDate: (Month month: 10 year: 1998).
	self assert: creditCard client = 'Mengana'.
	self assert: creditCard number = 1234567890123456.
	self assert: creditCard expiryDate = (Month month: 10 year: 1998).! !

!CreditCardTest methodsFor: 'tests' stamp: 'AC 11/16/2019 17:51:15'!
test02CantCreateCardWithEmptyName

	| name number date |
	
	name := ''.
	number := 1234567890123456.
	date := (Month month: 10 year: 1998).
	
	self should:[CreditCard forClient: name withNumber: number expiryDate: date.] raise: Error
	
	withExceptionDo: [:anError | self assert: CreditCard errorInvalidName equals: anError messageText].! !

!CreditCardTest methodsFor: 'tests' stamp: 'AC 11/16/2019 17:51:31'!
test03CantCreateCardWithNumberTooLongOrTooShort

	| name number date |
	
	name := 'Mengana'.
	number := 12345678901234567.
	date := (Month month: 10 year: 1998).
	
	self should:[CreditCard forClient: name withNumber: number expiryDate: date.] raise: Error
	
	withExceptionDo: [:anError | self assert: CreditCard errorInvalidNumber equals: anError messageText].
	
	
	number := 123456789012345.
	
	
	self should:[CreditCard forClient: name withNumber: number expiryDate: date.] raise: Error
	
	withExceptionDo: [:anError | self assert: CreditCard errorInvalidNumber equals: anError messageText].! !

!CreditCardTest methodsFor: 'tests' stamp: 'AC 11/16/2019 17:52:25'!
test04CanCheckIfExpired

	| name number date card |
	
	name := 'Mengana'.
	number := 1234567890123456.
	date := (Month month: 10 year: 1998).
	
	card := CreditCard forClient: name withNumber: number expiryDate: date.
	
	self assert: (card expiredOn: date).
	
	date := (Month month: 10 year: 1995).
	
	self deny: (card expiredOn: date).! !


!classDefinition: #RestTest category: #TusLibros!
TestCase subclass: #RestTest
	instanceVariableNames: 'rest testObjectFactory timer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:39:17'!
setUp

	testObjectFactory := StoreTestObjectsFactory new.
	
	timer := TimerMock new.

	
	rest := Rest withUserManager: UserManagerMock new withCatalog: testObjectFactory defaultCatalog withMP: MerchantProcessor new onMonthOfYear: testObjectFactory today withTimer: timer.
	
	! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:08:52'!
test01CantCreateCartWithInvalidUser
 
	self should: [rest createCartForClient: 'Ramiro' password: 'contraseņa correcta'.] raise: Error
	withExceptionDo: [:anError | self assert: Rest errorInvalidUser equals: anError messageText]
	
	! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:08:55'!
test02CanCreateCartWithValidUser

	|cartID emptyDictionary|
 
	cartID := rest createCartForClient: 'Gonzalo' password: 'contraseņa correcta'.
	
	
	emptyDictionary := Dictionary new.
	
	self assert: emptyDictionary = (rest listCart: cartID).! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:54:12'!
test03AddedItemsAppearOnCartList

	|cartID exemplaryList|
 
	cartID := rest createCartForClient: 'Gonzalo' password: 'contraseņa correcta'.
	
	rest addToCart: cartID book: testObjectFactory itemSoldByTheStore1 quantity: 3 clientPassword: 'contraseņa correcta'.
	
	exemplaryList := Dictionary new.
	
	exemplaryList at: testObjectFactory itemSoldByTheStore1 put: 3.
	
	self assert: exemplaryList = (rest listCart: cartID).! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:54:49'!
test04CannotAddItemOnInvalidCartID

	self should: [rest addToCart: 123 book: testObjectFactory itemSoldByTheStore1 quantity: 3 clientPassword: 'contraseņa correcta'] raise: Error
	withExceptionDo: [:anError | self assert: Rest errorInvalidCartId equals: anError messageText]! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:14:37'!
test05CannotCheckOutInvalidCart

	self should: [rest checkout: 123 withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa correcta' ] raise: Error
	withExceptionDo: [:anError | self assert: Rest errorInvalidCartId equals: anError messageText]

	! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:54:19'!
test06CheckoutChangesSaleHistoryCorrectly

	|cartID cart sale|
 
	cartID := rest createCartForClient: 'Gonzalo' password: 'contraseņa correcta'.
	
	rest addToCart: cartID book: testObjectFactory itemSoldByTheStore1 quantity: 3 clientPassword: 'contraseņa correcta'.
	cart := rest listCart: cartID.
	rest checkout: cartID withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa correcta' .
	
	sale := rest salesHistory at: 1.
	
	self assertSale: sale isTo: 'Gonzalo' for: (testObjectFactory itemSoldByTheStore1Price * 3) withItems: cart! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:54:26'!
test07CheckoutReturnsCorrectTransactionID

	|cartIDGonzalo cartIDMaria cartGonzalo cartMaria saleGonzalo transactionGonzalo transactionMaria saleMaria|
 
	cartIDGonzalo := rest createCartForClient: 'Gonzalo' password: 'contraseņa correcta'.
	
	rest addToCart: cartIDGonzalo book: testObjectFactory itemSoldByTheStore1 quantity: 3 clientPassword: 'contraseņa correcta'.
	cartGonzalo := rest listCart: cartIDGonzalo .
	
	transactionGonzalo := rest checkout: cartIDGonzalo withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa correcta' .
	
	
	
	cartIDMaria := rest createCartForClient: 'Maria' password: 'contraseņa correcta'.
	
	rest addToCart: cartIDMaria book: testObjectFactory itemSoldByTheStore2 quantity: 5 clientPassword: 'contraseņa correcta'.
	
	cartMaria := rest listCart: cartIDMaria .
	
	transactionMaria := rest checkout: cartIDMaria withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa correcta' .
	
	saleGonzalo := rest transactionNumber: transactionGonzalo .
	saleMaria := rest transactionNumber: transactionMaria .
	
	self assertSale: saleGonzalo isTo: 'Gonzalo' for: (testObjectFactory itemSoldByTheStore1Price * 3) withItems: cartGonzalo.
	self assertSale: saleMaria isTo: 'Maria' for: (testObjectFactory itemSoldByTheStore2Price * 5) withItems: cartMaria.! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:54:35'!
test08CanListDifferentClientsPurchases

	|cartIDGonzalo1 cartIDGonzalo2 cartIDMaria saleGonzalo1 saleGonzalo2 transactionGonzalo1 transactionGonzalo2 transactionMaria saleMaria|
 
	cartIDGonzalo1 := rest createCartForClient: 'Gonzalo' password: 'contraseņa correcta'.
	
	rest addToCart: cartIDGonzalo1 book: testObjectFactory itemSoldByTheStore1 quantity: 3 clientPassword: 'contraseņa correcta'.
	
	transactionGonzalo1 := rest checkout: cartIDGonzalo1 withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa correcta' .	
	cartIDGonzalo2 := rest createCartForClient: 'Gonzalo' password: 'contraseņa correcta'.
	
	rest addToCart: cartIDGonzalo2 book: testObjectFactory itemSoldByTheStore1 quantity: 79 clientPassword: 'contraseņa correcta'.
	
	transactionGonzalo2 := rest checkout: cartIDGonzalo2 withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa correcta' .
	
	cartIDMaria := rest createCartForClient: 'Maria' password: 'contraseņa correcta'.
	
	rest addToCart: cartIDMaria book: testObjectFactory itemSoldByTheStore2 quantity: 5 clientPassword: 'contraseņa correcta'.
	
	transactionMaria := rest checkout: cartIDMaria withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa correcta'  .
	
	saleGonzalo1 := rest transactionNumber: transactionGonzalo1 .
	saleGonzalo2 := rest transactionNumber: transactionGonzalo2 .
	saleMaria := rest transactionNumber: transactionMaria .
	
	self assert: ((rest listPurchasesFrom: 'Gonzalo') includes: saleGonzalo1).
	self assert: ((rest listPurchasesFrom: 'Gonzalo') includes: saleGonzalo2).
	self assert: ((rest listPurchasesFrom: 'Gonzalo') size = 2).
	self assert: ((rest listPurchasesFrom: 'Maria') includes: saleMaria).
	self assert: ((rest listPurchasesFrom: 'Maria') size = 1).! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:54:40'!
test09CantCheckoutSameCartTwice

	|cartID|
 
	cartID := rest createCartForClient: 'Gonzalo' password: 'contraseņa correcta'.
	
	rest addToCart: cartID book: testObjectFactory itemSoldByTheStore1 quantity: 3 clientPassword: 'contraseņa correcta'.
	
	rest checkout: cartID withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa correcta' .
	self should: [rest checkout: cartID withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa correcta'] raise: Error
	withExceptionDo: [:anError | self assert: Rest errorInvalidCartId equals: anError messageText]! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:52:38'!
test10CantAddToCartWithWrongPassword

	|cartID|
 
	cartID := rest createCartForClient: 'Gonzalo' password: 'contraseņa correcta'.
	
	self should: [rest addToCart: cartID book: testObjectFactory itemSoldByTheStore1 quantity: 3 clientPassword: 'contraseņa incorrecta' .] raise: Error
	withExceptionDo: [:anError | self assert: Rest errorInvalidPasswordForCart equals: anError messageText]! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:54:44'!
test11CantCheckoutCartWithWrongPassword

	|cartID|
 
	cartID := rest createCartForClient: 'Gonzalo' password: 'contraseņa correcta'.
	
	rest addToCart: cartID book: testObjectFactory itemSoldByTheStore1 quantity: 3 clientPassword: 'contraseņa correcta'.
	
	self should: [rest checkout: cartID withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa incorrecta' .] raise: Error
	withExceptionDo: [:anError | self assert: Rest errorInvalidPasswordForCart equals: anError messageText]! !

!RestTest methodsFor: 'tests' stamp: 'AC 11/18/2019 17:55:47'!
test12CartsAreInvalidAfter30Minutes

	|cartID|
 	timer setTime: 11.
	cartID := rest createCartForClient: 'Gonzalo' password: 'contraseņa correcta'.
	timer setTime: 41.
	
	self should: [rest addToCart: cartID book: testObjectFactory itemSoldByTheStore1 quantity: 3 clientPassword: 'contraseņa correcta'] raise: Error
	withExceptionDo: [:anError | self assert: Rest errorCartOutOfTTL equals: anError messageText].
	self should: [rest checkout: cartID withCreditCard: testObjectFactory notExpiredCreditCard clientPassword: 'contraseņa incorrecta' .] raise: Error
	withExceptionDo: [:anError | self assert: Rest errorCartOutOfTTL equals: anError messageText]! !


!RestTest methodsFor: 'support' stamp: 'AC 11/7/2019 20:15:39'!
assertSale: aSale isTo: aClient for: anAmmount withItems: aListOfItems

	self assert: aSale client = aClient.
	self assert: aSale ammount = anAmmount .
	self assert: aSale listOfItems = aListOfItems .! !


!classDefinition: #Cart category: #TusLibros!
Object subclass: #Cart
	instanceVariableNames: 'content catalog client timeOfCreation'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'observers' stamp: 'AC 11/16/2019 19:56:14'!
client
	
	^client! !

!Cart methodsFor: 'observers' stamp: 'AC 11/16/2019 18:09:17'!
costOfContent

	^content sum: [:aBook | catalog at: aBook] ifEmpty: [0].! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 00:50:14'!
hasItem: aBook

	^content includes: aBook! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 00:49:36'!
isEmpty

	^content isEmpty! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 12:51:47'!
list
	| list |
	
	list _ Dictionary new.
	content do: [:aBook | list at: aBook put: (content occurrencesOf: aBook)].
	^ list.! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 12:29:29'!
occurrencesOf: aBook

	^content occurrencesOf: aBook! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 00:49:44'!
size

	^content size! !

!Cart methodsFor: 'observers' stamp: 'AC 11/18/2019 17:49:52'!
timeOfCreation
	
	^timeOfCreation ! !


!Cart methodsFor: 'modifiers' stamp: 'AC 11/7/2019 12:50:05'!
add: aBook

	(catalog includesKey: aBook) ifFalse: [self error: self class errorCantAddBookOutOfCatalog ].

	content add: aBook! !

!Cart methodsFor: 'modifiers' stamp: 'AC 11/7/2019 00:59:52'!
add: aBook times: quantity

	|bookToAdd|
	
	bookToAdd := 0.
	
	quantity < 1 ifTrue: [self error: self class errorCantAddFewerThanOneBook ].

	[bookToAdd < quantity] whileTrue: [self add: aBook. bookToAdd := bookToAdd +1]! !


!Cart methodsFor: 'initialization' stamp: 'AC 11/7/2019 00:50:05'!
initialize

	content := OrderedCollection new! !

!Cart methodsFor: 'initialization' stamp: 'AC 11/18/2019 17:47:39'!
initializeAcceptingItemsOf: aCatalog forClient: aClient onTime: aTime

	timeOfCreation := aTime.
	client := aClient .
	catalog := aCatalog! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: #TusLibros!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'errors' stamp: 'AC 11/7/2019 12:42:08'!
errorCantAddBookOutOfCatalog

	^'That book is not sold by the store'! !

!Cart class methodsFor: 'errors' stamp: 'AC 11/7/2019 00:59:03'!
errorCantAddFewerThanOneBook

	^'Cant add fewer than one book to the cart'! !


!Cart class methodsFor: 'instance creation' stamp: 'AC 11/18/2019 17:47:51'!
acceptingItemsOf: aCatalog forClient: aClient onTime: aTime

	^Cart new initializeAcceptingItemsOf: aCatalog forClient: aClient onTime: aTime! !


!classDefinition: #Cashier category: #TusLibros!
Object subclass: #Cashier
	instanceVariableNames: 'cart creditCard salesHistory date merchantProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout' stamp: 'AC 11/16/2019 19:55:58'!
checkout

	|sale ammountToPay items |

	cart isEmpty ifTrue: [self error: self class errorCantCheckoutAnEmptyCart ].
	self validateCreditCard.
	
	ammountToPay := cart costOfContent .
	items := cart list.
	
	sale := Sale to: cart client for: ammountToPay of: items.
	
	merchantProcessor postWithAmount: ammountToPay withCreditCardNumber: creditCard number withExpiryDate: creditCard expiryDate withClient: creditCard client .
	
	salesHistory add: sale.! !


!Cashier methodsFor: 'initialization' stamp: 'AC 11/16/2019 19:53:51'!
initializeForChecking: aCart onDate: aDate withCreditCard: aCreditCard withSalesHistory: aSalesHistory withMP: aMerchantProcessor
	cart := aCart.
	date := aDate.
	creditCard := aCreditCard .
	salesHistory := aSalesHistory.
	merchantProcessor := aMerchantProcessor ! !


!Cashier methodsFor: 'validation' stamp: 'AC 11/7/2019 13:49:48'!
validateCreditCard

	(creditCard expiredOn: date) ifTrue: [self error: self class errorCantUseExpiredCard ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: #TusLibros!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'instance creation' stamp: 'AC 11/16/2019 19:53:59'!
forChecking: aCart onDate: aDate withCreditCard: aCreditCard withSalesHistory: aSalesHistory withMP: aMerchantProcessor

	^Cashier new initializeForChecking: aCart onDate: aDate withCreditCard: aCreditCard withSalesHistory: aSalesHistory withMP: aMerchantProcessor! !


!Cashier class methodsFor: 'errors' stamp: 'AC 11/7/2019 12:19:23'!
errorCantCheckoutAnEmptyCart

	^'Cant Checkout an empty cart'! !

!Cashier class methodsFor: 'errors' stamp: 'AC 11/7/2019 13:42:10'!
errorCantUseExpiredCard
		
	^'The Credit Card is Expired'! !


!classDefinition: #CreditCard category: #TusLibros!
Object subclass: #CreditCard
	instanceVariableNames: 'nameOfClient number expiryDate'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'observers' stamp: 'AC 11/7/2019 01:25:08'!
client

	^nameOfClient ! !

!CreditCard methodsFor: 'observers' stamp: 'AC 11/16/2019 17:56:48'!
expiredOn: aDate


	^ (expiryDate yearNumber < aDate yearNumber) or: (
		(expiryDate yearNumber = aDate yearNumber) and: (expiryDate monthIndex <= aDate monthIndex)
	)	! !

!CreditCard methodsFor: 'observers' stamp: 'AC 11/7/2019 01:24:55'!
expiryDate

	^expiryDate ! !

!CreditCard methodsFor: 'observers' stamp: 'AC 11/7/2019 01:25:18'!
number

	^number! !


!CreditCard methodsFor: 'initializers' stamp: 'AC 11/7/2019 01:38:32'!
initializeForClient: aName withNumber: aNumber expiryDate: aDate

	aName isEmpty ifTrue: [self error: self class errorInvalidName].
	
	aNumber decimalDigitLength = 16 ifFalse: [self error: self class errorInvalidNumber].

	nameOfClient := aName.
	number := aNumber.
	expiryDate := aDate! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: #TusLibros!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'AC 11/7/2019 01:29:25'!
forClient: aName withNumber: aNumber expiryDate: aDate

	^CreditCard new initializeForClient: aName withNumber: aNumber expiryDate: aDate! !


!CreditCard class methodsFor: 'errors' stamp: 'AC 11/7/2019 01:37:07'!
errorInvalidName

	^'Invalid Name'! !

!CreditCard class methodsFor: 'errors' stamp: 'AC 11/7/2019 01:38:45'!
errorInvalidNumber

	^'Invalid Credit Card Number'! !


!classDefinition: #MerchantProcessor category: #TusLibros!
Object subclass: #MerchantProcessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessor methodsFor: 'interface' stamp: 'AC 11/7/2019 13:50:36'!
postWithAmount: anAmount withCreditCardNumber: aCreditCardnumber withExpiryDate: aMonthOfYear withClient: aName

	^true! !


!classDefinition: #Rest category: #TusLibros!
Object subclass: #Rest
	instanceVariableNames: 'userManager carts catalog date salesHistory merchantProcessor nextCartID timer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Rest methodsFor: 'cart' stamp: 'AC 11/18/2019 17:56:43'!
addToCart: aCartID book: aBook quantity: aQuantity clientPassword: givenPassword

	self validateCart: aCartID withPassword: givenPassword.
	(carts at: aCartID) add: aBook times: aQuantity .! !

!Rest methodsFor: 'cart' stamp: 'AC 11/18/2019 17:56:51'!
checkout: aCartID withCreditCard: aCreditCard clientPassword: givenPassword

	|cashier cart|

	self validateCart: aCartID withPassword: givenPassword.
	
	
	cart := carts at: aCartID .
	
	cashier := Cashier forChecking: cart onDate: date withCreditCard: aCreditCard withSalesHistory: salesHistory withMP: merchantProcessor .
	
	cashier checkout.
	
	carts removeKey: aCartID.
	
	^salesHistory size! !

!Rest methodsFor: 'cart' stamp: 'AC 11/18/2019 17:48:34'!
createCartForClient: aClient password: aPassword

	|newCartID|
	
	(userManager validUser: aClient withPassword: aPassword) ifFalse: [self error: self class errorInvalidUser ].
	
	newCartID := nextCartID .
	
	nextCartID := nextCartID + 1.
	carts at: newCartID put: (Cart acceptingItemsOf: catalog forClient: aClient onTime: timer checkTime ).
	^newCartID
	
	! !

!Rest methodsFor: 'cart' stamp: 'AC 11/7/2019 20:00:21'!
listCart: aCartId

	^(carts at: aCartId) list! !


!Rest methodsFor: 'initialization' stamp: 'AC 11/18/2019 17:42:40'!
initializedWithUserManager: anUserManager withCatalog: aCatalog withMP: aMerchantProcessor onMonthOfYear: aMonthOfYear withTimer: aTimer

	timer := aTimer.
	nextCartID := 1.
	date := aMonthOfYear .
	
	carts := Dictionary new.
	userManager := anUserManager .
	catalog := aCatalog.
	
	merchantProcessor := aMerchantProcessor .
	
	salesHistory := OrderedCollection new.! !


!Rest methodsFor: 'validation' stamp: 'AC 11/18/2019 17:56:27'!
validateCart: aCartID withPassword: givenPassword

	self validateCartId: aCartID .
	
	self validateCartTimeToLive: aCartID.
	
	self validatePassword: givenPassword forCart: aCartID.! !

!Rest methodsFor: 'validation' stamp: 'AC 11/16/2019 19:58:16'!
validateCartId: aCartID

	(carts includesKey: aCartID)  ifFalse: [ self error: self class errorInvalidCartId ]! !

!Rest methodsFor: 'validation' stamp: 'AC 11/18/2019 17:51:00'!
validateCartTimeToLive: aCartID

	|cart|
	
	cart := carts at: aCartID.

	(timer wasOver30MinutesAgo: cart timeOfCreation) ifTrue: [self error: self class errorCartOutOfTTL ]! !

!Rest methodsFor: 'validation' stamp: 'AC 11/18/2019 17:15:41'!
validatePassword: givenPassword forCart: aValidCartID

	|cart| 
	
	cart := carts at: aValidCartID .
	
	(userManager validUser: cart client withPassword: givenPassword) ifFalse: [self error: self class errorInvalidPasswordForCart ].! !


!Rest methodsFor: 'observers' stamp: 'AC 11/16/2019 20:56:04'!
listPurchasesFrom: aClient

	|purchases|
	
	purchases := OrderedCollection new.
	
	salesHistory do: [:sale | 
		(sale client = aClient) ifTrue: [purchases add: sale]
		].
	
	^purchases! !

!Rest methodsFor: 'observers' stamp: 'AC 11/7/2019 20:42:18'!
salesHistory

	^salesHistory ! !

!Rest methodsFor: 'observers' stamp: 'AC 11/16/2019 20:29:45'!
transactionNumber: transactionID

	^salesHistory at: transactionID ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Rest class' category: #TusLibros!
Rest class
	instanceVariableNames: ''!

!Rest class methodsFor: 'errors' stamp: 'AC 11/18/2019 17:41:39'!
errorCartOutOfTTL

	^'Cart was created over 30 minutes ago'! !

!Rest class methodsFor: 'errors' stamp: 'AC 11/7/2019 20:03:20'!
errorInvalidCartId

	^'Invalid Cart ID'! !

!Rest class methodsFor: 'errors' stamp: 'AC 11/18/2019 17:05:05'!
errorInvalidPasswordForCart

	^'Invalid Password For That Cart'! !

!Rest class methodsFor: 'errors' stamp: 'AC 11/7/2019 19:31:41'!
errorInvalidUser

	^'Invalid Username'! !


!Rest class methodsFor: 'instance creation' stamp: 'AC 11/18/2019 17:41:59'!
withUserManager: anUserManager withCatalog: aCatalog withMP: aMerchantProcessor onMonthOfYear: aMonthOfYear withTimer: aTimer

	^Rest new initializedWithUserManager: anUserManager withCatalog: aCatalog  withMP: aMerchantProcessor onMonthOfYear: aMonthOfYear withTimer: aTimer! !


!classDefinition: #Sale category: #TusLibros!
Object subclass: #Sale
	instanceVariableNames: 'client ammount listOfItemsSold'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'initialization' stamp: 'AC 11/7/2019 13:30:23'!
initializeTo: aClient for: anAmmount of: aListOfItemsSold

	client := aClient .
	ammount := anAmmount .
	listOfItemsSold := aListOfItemsSold ! !


!Sale methodsFor: 'observers' stamp: 'AC 11/7/2019 13:31:51'!
ammount

	^ammount! !

!Sale methodsFor: 'observers' stamp: 'AC 11/7/2019 13:31:37'!
client

	^client! !

!Sale methodsFor: 'observers' stamp: 'AC 11/7/2019 13:32:02'!
listOfItems

	^listOfItemsSold ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: #TusLibros!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'instance creation' stamp: 'AC 11/7/2019 13:31:24'!
to: aClient for: anAmmount of: aListOfItemsSold

	^Sale new initializeTo: aClient for: anAmmount of: aListOfItemsSold.
! !


!classDefinition: #StoreTestObjectsFactory category: #TusLibros!
Object subclass: #StoreTestObjectsFactory
	instanceVariableNames: 'today'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'AC 11/16/2019 17:43:03'!
itemNotSoldByTheStore
	
	^'invalidBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'AC 11/16/2019 17:43:36'!
itemSoldByTheStore1
	
	^ 'validBook1'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'AC 11/16/2019 18:10:26'!
itemSoldByTheStore1Price
	
	^1*peso! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'AC 11/16/2019 17:43:22'!
itemSoldByTheStore2
	
	^ 'validBook2'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'AC 11/16/2019 18:10:22'!
itemSoldByTheStore2Price
	
	^2*peso! !


!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'AC 11/18/2019 17:48:09'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog forClient: 'Mengana' onTime: 0.! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'AC 11/16/2019 18:02:27'!
defaultCatalog
	
	^ Dictionary new
		at: self itemSoldByTheStore1 put: self itemSoldByTheStore1Price;
		at: self itemSoldByTheStore2 put: self itemSoldByTheStore2Price;
		
		yourself ! !


!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'AC 11/16/2019 17:45:11'!
expiredCreditCard
	
	^CreditCard forClient: 'Mengana' withNumber: 1234567890123456 expiryDate:  (Month month: today monthIndex year: today yearNumber - 1)! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'AC 11/16/2019 17:45:02'!
notExpiredCreditCard
	
	^CreditCard forClient: 'Mengana' withNumber: 1234567890123456 expiryDate: (Month month: today monthIndex year: today yearNumber + 1)! !


!StoreTestObjectsFactory methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:37'!
initialize

	today := DateAndTime now! !


!StoreTestObjectsFactory methodsFor: 'date' stamp: 'HernanWilkinson 6/17/2013 18:37'!
today
	
	^ today! !


!classDefinition: #TimerMock category: #TusLibros!
Object subclass: #TimerMock
	instanceVariableNames: 'time'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TimerMock methodsFor: 'initialization' stamp: 'AC 11/18/2019 17:38:04'!
initialize

	time := 0! !


!TimerMock methodsFor: 'setter' stamp: 'AC 11/18/2019 17:43:59'!
setTime: aTime

	time := aTime! !


!TimerMock methodsFor: 'observer' stamp: 'AC 11/18/2019 17:44:21'!
checkTime

	^time! !

!TimerMock methodsFor: 'observer' stamp: 'AC 11/18/2019 17:45:20'!
wasOver30MinutesAgo: aTime

	^(time - aTime) >= 30! !


!classDefinition: #UserManagerMock category: #TusLibros!
Object subclass: #UserManagerMock
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!UserManagerMock methodsFor: 'interface' stamp: 'AC 11/18/2019 17:08:36'!
validUser: aClient withPassword: aPassword

	aClient = 'Ramiro' ifTrue:[^false].
	^aPassword = 'contraseņa correcta'! !
