!classDefinition: #CartTest category: #TusLibros!
TestCase subclass: #CartTest
	instanceVariableNames: 'cart catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:55:34'!
setUp
	
	catalog := Dictionary new.
	catalog at: self itemSoldByStore1 put: 1*peso.
	catalog at: self itemSoldByStore2 put: 2*peso.
	
	cart := Cart acceptingItemsOf: catalog.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:46:47'!
test01NewCartIsEmpty
	
	self assert: cart isEmpty.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:48:10'!
test02CartIsNotEmptyAfterAddingBook
	
	cart add: self itemSoldByStore1.
	
	self deny: cart isEmpty.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:34:41'!
test03NewCartHasZeroSize
	
	self assert: cart size = 0! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:48:17'!
test04CartSizeIsOneAfterAddingBook
	
	cart add: self itemSoldByStore1.
	
	self assert: cart size = 1! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:48:25'!
test05CartHasBookThatWasAdded
	
	self deny: (cart hasItem: self itemSoldByStore1).
	
	cart add: self itemSoldByStore1.
	
	self assert: (cart hasItem: self itemSoldByStore1).! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:48:46'!
test06CartHasBooksAddedAndNotOthers
	
	self deny: (cart hasItem: self itemSoldByStore1).
	self deny: (cart hasItem: self itemSoldByStore2).
	
	cart add: self itemSoldByStore1.
	
	self assert: (cart hasItem: self itemSoldByStore1).
	self deny: (cart hasItem: self itemSoldByStore2).
	
	cart add: self itemSoldByStore2.
	
	self assert: (cart hasItem: self itemSoldByStore1).
	self assert: (cart hasItem: self itemSoldByStore2).! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:48:50'!
test07CanAddMultipleBooksCorrectly
	
	cart add: self itemSoldByStore1 times: 5.
	
	self assert: (cart hasItem: self itemSoldByStore1).
	self assert: cart size = 5! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:48:54'!
test08CantAddFewerThanOneBook
	
	self should: [cart add: self itemSoldByStore1 times: 0] raise: Error
	 withExceptionDo: [:anError | self assert: anError messageText equals: Cart errorCantAddFewerThanOneBook. 
		self assert: cart isEmpty].! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:49:05'!
test09CanCheckOccurrences
	
	self assert: (cart occurrencesOf: self itemSoldByStore1) = 0.
	
	cart add: self itemSoldByStore1 times: 8.
	
	self assert: (cart occurrencesOf: self itemSoldByStore1) = 8.
	
	cart add: self itemSoldByStore1 times: 7.
	
	self assert: (cart occurrencesOf: self itemSoldByStore1) = 15.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:41:53'!
test10CantAddBookThatIsntInCatalog


	self should: [cart add: self itemNotSoldByStore ] raise: Error
	withExceptionDo: [:anError | self assert: anError messageText equals: Cart errorCantAddBookOutOfCatalog.]! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 12:55:07'!
test11CanListCorrectly

	|list|

	cart add: self itemSoldByStore1 times: 5.
	cart add: self itemSoldByStore2 times: 7.
	
	list := cart list.
	
	self assert: (list at: self itemSoldByStore1) = 5.
	self assert: (list at: self itemSoldByStore2) = 7.! !

!CartTest methodsFor: 'tests' stamp: 'AC 11/7/2019 13:00:56'!
test12CanObserveTotalCostOfContent

	cart add: self itemSoldByStore1 times: 5.
	cart add: self itemSoldByStore2 times: 7.
	
	self assert: cart costOfContent = (19*peso).! !


!CartTest methodsFor: 'support' stamp: 'AC 11/7/2019 12:38:11'!
itemNotSoldByStore

	^'item not sold'! !

!CartTest methodsFor: 'support' stamp: 'AC 11/7/2019 12:38:18'!
itemSoldByStore1

	^'item1'! !

!CartTest methodsFor: 'support' stamp: 'AC 11/7/2019 12:38:24'!
itemSoldByStore2

	^'item2'! !


!classDefinition: #CashierTest category: #TusLibros!
TestCase subclass: #CashierTest
	instanceVariableNames: 'cart creditCard salesHistory todayDate merchantProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'AC 11/7/2019 13:55:32'!
setUp

	cart := Cart acceptingItemsOf: self defaultCatalog .
	creditCard := CreditCard forClient: 'Mengana' withNumber: 1234567890123456 expiryDate: (GregorianMonthOfYear januaryOf: 1998).
	
	todayDate := GregorianMonthOfYear januaryOf: 1997.
	
	salesHistory := OrderedCollection new.
	
	merchantProcessor := MerchantProcessor new.
	
	! !

!CashierTest methodsFor: 'tests' stamp: 'AC 11/7/2019 13:08:00'!
test01CantCheckoutEmptyCart

	| cashier | 
	
	cashier := self createCashier.
	
	
	self should: [cashier checkout] raise: Error
	withExceptionDo: [:anError | self assert: Cashier errorCantCheckoutAnEmptyCart equals: anError messageText.]! !

!CashierTest methodsFor: 'tests' stamp: 'AC 11/7/2019 13:39:03'!
test02CheckoutRegistersSale

	| cashier sale | 
	
	cashier := self createCashier.
	
	cart add: self itemSoldByStore1.
	
	self assert: salesHistory isEmpty.
	
	cashier checkout.
	
	sale := salesHistory at: 1.
	
	self assertSale: sale isTo: 'Mengana' for: (1*peso) withItems: cart list.! !

!CashierTest methodsFor: 'tests' stamp: 'AC 11/7/2019 13:41:49'!
test03CantCheckoutWithExpiredCreditCard

	| cashier sale | 
	
	creditCard := CreditCard forClient: 'Mengana' withNumber: 1234567890123456 expiryDate: (GregorianMonthOfYear juneOf: 1990).
	
	cashier := self createCashier.
	
	cart add: self itemSoldByStore1.
	
	self should: [cashier checkout] raise: Error
	withExceptionDo: [:anError | self assert: Cashier errorCantUseExpiredCard equals: anError messageText.]! !


!CashierTest methodsFor: 'support' stamp: 'AC 11/7/2019 13:35:37'!
assertSale: aSale isTo: aClient for: anAmmount withItems: aListOfItems

	self assert: aSale client = aClient.
	self assert: aSale ammount = anAmmount .
	self assert: aSale listOfItems = aListOfItems .! !

!CashierTest methodsFor: 'support' stamp: 'AC 11/7/2019 12:15:37'!
createCashier

	^Cashier forChecking: cart onDate: todayDate withCreditCard: creditCard withSalesHistory: salesHistory withMP: merchantProcessor.! !

!CashierTest methodsFor: 'support' stamp: 'AC 11/7/2019 13:06:34'!
defaultCatalog
	
	|catalog|
	
	catalog := Dictionary new.
	catalog at: self itemSoldByStore1 put: 1*peso.
	catalog at: self itemSoldByStore2 put: 2*peso.
	
	^catalog! !

!CashierTest methodsFor: 'support' stamp: 'AC 11/7/2019 13:04:02'!
itemNotSoldByTheStore
	
	^'invalidBook'! !

!CashierTest methodsFor: 'support' stamp: 'AC 11/7/2019 12:38:18'!
itemSoldByStore1
	
	^ 'validBook1'! !

!CashierTest methodsFor: 'support' stamp: 'AC 11/7/2019 12:38:24'!
itemSoldByStore2
	
	^ 'validBook2'! !


!classDefinition: #CreditCardTest category: #TusLibros!
TestCase subclass: #CreditCardTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCardTest methodsFor: 'tests' stamp: 'AC 11/7/2019 01:34:05'!
test01FieldsCanBeObserved

	|creditCard name number date |
	
	name := 'Mengana'.
	number := 1234567890123456.
	date := GregorianMonthOfYear januaryOf: 2006.
	
	
	creditCard := CreditCard forClient: name withNumber: number expiryDate: date.
	
	self assert: creditCard client = name.
	self assert: creditCard number = number.
	self assert: creditCard expiryDate = date.! !

!CreditCardTest methodsFor: 'tests' stamp: 'AC 11/7/2019 01:36:26'!
test02CantCreateCardWithEmptyName

	| name number date |
	
	name := ''.
	number := 1234567890123456.
	date := GregorianMonthOfYear januaryOf: 2006.
	
	self should:[CreditCard forClient: name withNumber: number expiryDate: date.] raise: Error
	
	withExceptionDo: [:anError | self assert: CreditCard errorInvalidName equals: anError messageText].! !

!CreditCardTest methodsFor: 'tests' stamp: 'AC 11/7/2019 01:39:11'!
test03CantCreateCardWithNumberTooLongOrTooShort

	| name number date |
	
	name := 'Mengana'.
	number := 12345678901234567.
	date := GregorianMonthOfYear januaryOf: 2006.
	
	self should:[CreditCard forClient: name withNumber: number expiryDate: date.] raise: Error
	
	withExceptionDo: [:anError | self assert: CreditCard errorInvalidNumber equals: anError messageText].
	
	
	number := 123456789012345.
	
	
	self should:[CreditCard forClient: name withNumber: number expiryDate: date.] raise: Error
	
	withExceptionDo: [:anError | self assert: CreditCard errorInvalidNumber equals: anError messageText].! !

!CreditCardTest methodsFor: 'tests' stamp: 'AC 11/7/2019 13:46:54'!
test04CanCheckIfExpired

	| name number date card |
	
	name := 'Mengana'.
	number := 1234567890123456.
	date := GregorianMonthOfYear januaryOf: 2006.
	
	card := CreditCard forClient: name withNumber: number expiryDate: date.
	
	self assert: (card expiredOn: date).
	
	date := GregorianMonthOfYear januaryOf: 2004.
	
	self deny: (card expiredOn: date).! !


!classDefinition: #Cart category: #TusLibros!
Object subclass: #Cart
	instanceVariableNames: 'content catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 13:00:36'!
costOfContent

	^content sum: [:aBook | catalog at: aBook] ifEmpty: [0].! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 00:50:14'!
hasItem: aBook

	^content includes: aBook! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 00:49:36'!
isEmpty

	^content isEmpty! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 12:51:47'!
list
	| list |
	
	list _ Dictionary new.
	content do: [:aBook | list at: aBook put: (content occurrencesOf: aBook)].
	^ list.! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 12:29:29'!
occurrencesOf: aBook

	^content occurrencesOf: aBook! !

!Cart methodsFor: 'observers' stamp: 'AC 11/7/2019 00:49:44'!
size

	^content size! !


!Cart methodsFor: 'modifiers' stamp: 'AC 11/7/2019 12:50:05'!
add: aBook

	(catalog includesKey: aBook) ifFalse: [self error: self class errorCantAddBookOutOfCatalog ].

	content add: aBook! !

!Cart methodsFor: 'modifiers' stamp: 'AC 11/7/2019 00:59:52'!
add: aBook times: quantity

	|bookToAdd|
	
	bookToAdd := 0.
	
	quantity < 1 ifTrue: [self error: self class errorCantAddFewerThanOneBook ].

	[bookToAdd < quantity] whileTrue: [self add: aBook. bookToAdd := bookToAdd +1]! !


!Cart methodsFor: 'initialization' stamp: 'AC 11/7/2019 00:50:05'!
initialize

	content := OrderedCollection new! !

!Cart methodsFor: 'initialization' stamp: 'AC 11/7/2019 12:43:31'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: #TusLibros!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'errors' stamp: 'AC 11/7/2019 12:42:08'!
errorCantAddBookOutOfCatalog

	^'That book is not sold by the store'! !

!Cart class methodsFor: 'errors' stamp: 'AC 11/7/2019 00:59:03'!
errorCantAddFewerThanOneBook

	^'Cant add fewer than one book to the cart'! !


!Cart class methodsFor: 'as yet unclassified' stamp: 'AC 11/7/2019 12:43:10'!
acceptingItemsOf: aCatalog

	^Cart new initializeAcceptingItemsOf: aCatalog! !


!classDefinition: #Cashier category: #TusLibros!
Object subclass: #Cashier
	instanceVariableNames: 'cart creditCard salesHistory date merchantProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout' stamp: 'AC 11/7/2019 13:53:23'!
checkout

	|sale client ammountToPay items |

	cart isEmpty ifTrue: [self error: self class errorCantCheckoutAnEmptyCart ].
	self validateCreditCard.
	
	client:= creditCard client.
	ammountToPay := cart costOfContent .
	items := cart list.
	
	sale := Sale to: client for: ammountToPay of: items.
	
	merchantProcessor postWithAmount: ammountToPay withCreditCardNumber: creditCard number withExpiryDate: creditCard expiryDate withClient: client.
	
	salesHistory add: sale.! !


!Cashier methodsFor: 'initialization' stamp: 'AC 11/7/2019 14:05:06'!
initializeForChecking: aCart onDate: aDate withCreditCard: aCreditCard withSalesHistory: aSalesHistory withMP: aMerchantProcessor

	cart := aCart.
	date := aDate.
	creditCard := aCreditCard .
	salesHistory := aSalesHistory.
	merchantProcessor := aMerchantProcessor ! !


!Cashier methodsFor: 'validation' stamp: 'AC 11/7/2019 13:49:48'!
validateCreditCard

	(creditCard expiredOn: date) ifTrue: [self error: self class errorCantUseExpiredCard ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: #TusLibros!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'instance creation' stamp: 'AC 11/7/2019 13:12:41'!
forChecking: aCart onDate: aDate withCreditCard: aCreditCard withSalesHistory: aSalesHistory withMP: aMerchantProcessor

	^Cashier new initializeForChecking: aCart onDate: aDate withCreditCard: aCreditCard withSalesHistory: aSalesHistory withMP: aMerchantProcessor! !


!Cashier class methodsFor: 'errors' stamp: 'AC 11/7/2019 12:19:23'!
errorCantCheckoutAnEmptyCart

	^'Cant Checkout an empty cart'! !

!Cashier class methodsFor: 'errors' stamp: 'AC 11/7/2019 13:42:10'!
errorCantUseExpiredCard
		
	^'The Credit Card is Expired'! !


!classDefinition: #CreditCard category: #TusLibros!
Object subclass: #CreditCard
	instanceVariableNames: 'nameOfClient number expiryDate'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'observers' stamp: 'AC 11/7/2019 01:25:08'!
client

	^nameOfClient ! !

!CreditCard methodsFor: 'observers' stamp: 'AC 11/7/2019 13:48:08'!
expiredOn: aDate
	
	^ expiryDate  <= aDate. 
! !

!CreditCard methodsFor: 'observers' stamp: 'AC 11/7/2019 01:24:55'!
expiryDate

	^expiryDate ! !

!CreditCard methodsFor: 'observers' stamp: 'AC 11/7/2019 01:25:18'!
number

	^number! !


!CreditCard methodsFor: 'initializers' stamp: 'AC 11/7/2019 01:38:32'!
initializeForClient: aName withNumber: aNumber expiryDate: aDate

	aName isEmpty ifTrue: [self error: self class errorInvalidName].
	
	aNumber decimalDigitLength = 16 ifFalse: [self error: self class errorInvalidNumber].

	nameOfClient := aName.
	number := aNumber.
	expiryDate := aDate! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: #TusLibros!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'AC 11/7/2019 01:29:25'!
forClient: aName withNumber: aNumber expiryDate: aDate

	^CreditCard new initializeForClient: aName withNumber: aNumber expiryDate: aDate! !


!CreditCard class methodsFor: 'errors' stamp: 'AC 11/7/2019 01:37:07'!
errorInvalidName

	^'Invalid Name'! !

!CreditCard class methodsFor: 'errors' stamp: 'AC 11/7/2019 01:38:45'!
errorInvalidNumber

	^'Invalid Credit Card Number'! !


!classDefinition: #MerchantProcessor category: #TusLibros!
Object subclass: #MerchantProcessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessor methodsFor: 'interface' stamp: 'AC 11/7/2019 13:50:36'!
postWithAmount: anAmount withCreditCardNumber: aCreditCardnumber withExpiryDate: aMonthOfYear withClient: aName

	^true! !


!classDefinition: #Sale category: #TusLibros!
Object subclass: #Sale
	instanceVariableNames: 'client ammount listOfItemsSold'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'initialization' stamp: 'AC 11/7/2019 13:30:23'!
initializeTo: aClient for: anAmmount of: aListOfItemsSold

	client := aClient .
	ammount := anAmmount .
	listOfItemsSold := aListOfItemsSold ! !


!Sale methodsFor: 'observers' stamp: 'AC 11/7/2019 13:31:51'!
ammount

	^ammount! !

!Sale methodsFor: 'observers' stamp: 'AC 11/7/2019 13:31:37'!
client

	^client! !

!Sale methodsFor: 'observers' stamp: 'AC 11/7/2019 13:32:02'!
listOfItems

	^listOfItemsSold ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: #TusLibros!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'instance creation' stamp: 'AC 11/7/2019 13:31:24'!
to: aClient for: anAmmount of: aListOfItemsSold

	^Sale new initializeTo: aClient for: anAmmount of: aListOfItemsSold.
! !
